﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TexFactory.Controls;

namespace TexFactory
{
    public partial class Window : Form
    {
        private const string SupportedTexturesFilter = "Supported Textures (*.png, *.tga, *.dds)|*.png;*.tga;*.dds";
        private const string PNGFilter = "PNG Images (*.png)|*.png";
        private const string TGAFilter = "Truevision TGA Images (*.tga)|*.tga";
        private const string DDSFilter = "DirectDraw Surface Images (*.dds)|*.dds";

        public bool IsConverting { get; private set; } = false;

        private bool CancelRemainingConversions = false;
        private List<object> RawFormats = new List<object>();
        private List<object> sRGBFormats = new List<object>();

        public Window()
        {
            InitializeComponent();
            foreach (SharpDX.DXGI.Format format in Enum.GetValues(typeof(SharpDX.DXGI.Format)))
            {
                if (format != SharpDX.DXGI.Format.Unknown)
                {
                    if (SharpDX.DXGI.FormatHelper.IsVideo(format))
                        continue;

                    if (SharpDX.DXGI.FormatHelper.IsSRgb(format))
                        sRGBFormats.Add(format);
                    else
                        RawFormats.Add(format);
                    //OutputFormatComboBox.Items.Add(format);
                }
            }
            AdapterComboBox.Items.Add("No Hardware Acceleration");
            AdapterComboBox.Items.AddRange(Win32.EnumerateVideoAdapters().ToArray());
            try
            {
                AdapterComboBox.SelectedIndex = Int32.Parse(Program.Config.GetValueOrDefault("Conversion", "GPUChoice", "1"));
            }
            catch
            {
                AdapterComboBox.SelectedIndex = 1;
                Logger.WriteLine("[WARNING]: Couldn't parse preferred adapter index from '" + Program.Config.GetValueOrDefault("Conversion", "GPUChoice", "1") + "'!");
            }
            RemoveSuccessfulFilesCheckBox.Checked = Program.Config.GetValueOrDefault("Conversion", "RemoveSuccessfulFiles", "False").Equals("true", StringComparison.CurrentCultureIgnoreCase);
        }

        public void AddFile(string filename, string outputFilename)
        {
            TextureItem newItem = new TextureItem(filename, outputFilename);
            FileList.Items.Add(newItem);
            FileList.SelectedItem = newItem;
            ConvertButton.Enabled = true;
            ConvertButton.Text = "Convert " + FileList.Items.Count + " Files";
        }

        public void AddFiles(IEnumerable<string> filenames)
        {
            FileList.BeginUpdate();
            foreach (string filename in filenames)
            {
                if (filename.EndsWith(".png", StringComparison.CurrentCultureIgnoreCase)
                    || filename.EndsWith(".tga", StringComparison.CurrentCultureIgnoreCase))
                {
                    AddFile(filename, System.IO.Path.ChangeExtension(filename, ".dds"));
                }
                else if (filename.EndsWith(".dds", StringComparison.CurrentCultureIgnoreCase))
                {
                    AddFile(filename, System.IO.Path.ChangeExtension(filename, ".png"));
                }
            }
            FileList.EndUpdate();
        }

        public void LoadOptionsPanel(TextureItem item)
        {
            SharpDX.DXGI.Format format = item.OutputFormat; // We need to save this so resetting the output format combo box doesn't change it
            OptionsPanel.Visible = true;
            SelectedFileLabel.Text = "Input:       " + item.Filename;
            OutputFormatComboBox.Items.Clear();
            OutputFormatComboBox.Items.AddRange(item.IsOutputSRGB ? sRGBFormats.ToArray() : RawFormats.ToArray());
            OutputFormatComboBox.SelectedItem = (format == SharpDX.DXGI.Format.Unknown) ? null : (object)format;
            InputSRGBCheckBox.Checked = item.IsInputSRGB;
            OutputSRGBCheckBox.Checked = item.IsOutputSRGB;
            OutputFormatComboBox.SelectedItem = (format == SharpDX.DXGI.Format.Unknown) ? null : (object)format;
            OutputFormatComboBox.Enabled = true; //item.OutputFilename.EndsWith(".dds", StringComparison.CurrentCultureIgnoreCase);
            OutputFilenameTextBox.Text = item.OutputFilename;
            GenerateMipmapsCheckBox.Checked = item.GenerateMipmaps;
            GenerateMipmapsCheckBox.Enabled = item.OutputFilename.EndsWith(".dds", StringComparison.CurrentCultureIgnoreCase);
            ResizeCheckBox.Checked = item.Resize;
            WidthTextBox.Text = item.ResizeWidth.ToString();
            HeightTextBox.Text = item.ResizeHeight.ToString();
            WidthTextBox.Enabled = ResizeCheckBox.Checked;
            HeightTextBox.Enabled = ResizeCheckBox.Checked;
        }

        private void Window_Load(object sender, EventArgs e)
        {
            Text += " v" + Program.VersionString;

            if (Program.Config.GetValueOrDefault("Window", "IsMaximized", "False").Equals("True", StringComparison.InvariantCultureIgnoreCase))
            {
                WindowState = FormWindowState.Maximized;
            }
            else
            {
                if (Int32.TryParse(Program.Config.GetValueOrDefault("Window", "X", Left.ToString()), out int newLeft))
                {
                    Left = newLeft;
                }
                if (Int32.TryParse(Program.Config.GetValueOrDefault("Window", "Y", Top.ToString()), out int newTop))
                {
                    Top = newTop;
                }
                if (Int32.TryParse(Program.Config.GetValueOrDefault("Window", "Width", Width.ToString()), out int newWidth))
                {
                    Width = newWidth;
                }
                if (Int32.TryParse(Program.Config.GetValueOrDefault("Window", "Height", Height.ToString()), out int newHeight))
                {
                    Height = newHeight;
                }
            }
        }

        private void AddFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            // TODO: Restore directory: openDialog.InitialDirectory = 
            openDialog.Filter = SupportedTexturesFilter + "|" + PNGFilter + "|" + TGAFilter + "|" + DDSFilter;
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = PNGFilter + "|" + TGAFilter + "|" + DDSFilter;
                saveDialog.InitialDirectory = System.IO.Path.GetDirectoryName(openDialog.FileName);
                saveDialog.FileName = System.IO.Path.GetFileNameWithoutExtension(openDialog.FileName);
                // Try to guess the format they want
                if (openDialog.FileName.EndsWith(".dds", StringComparison.CurrentCultureIgnoreCase))
                {
                    saveDialog.FilterIndex = 1;
                }
                else
                {
                    saveDialog.FilterIndex = 3;
                }

                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    AddFile(openDialog.FileName, saveDialog.FileName);
                }
            }
        }

        private void FileList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!IsConverting)
            {
                RemoveFileButton.Enabled = FileList.SelectedIndex != -1;
                if (FileList.SelectedIndex > -1)
                    LoadOptionsPanel(FileList.SelectedItem as TextureItem);
            }
        }

        private void RemoveFileButton_Click(object sender, EventArgs e)
        {
            if (FileList.SelectedIndex == -1)
                return;

            int selectedIndex = FileList.SelectedIndex;
            FileList.Items.Remove(FileList.SelectedItem);
            if (selectedIndex > 0)
                selectedIndex--;
            if (FileList.Items.Count > 0)
                FileList.SelectedIndex = selectedIndex;
            ConvertButton.Enabled = OptionsPanel.Visible = FileList.Items.Count > 0;
            ConvertButton.Text = "Convert " + FileList.Items.Count + " Files";

            // For some reason the ListBox won't automagically scroll up when we make room at the bottom of the ListBox.
            if (FileList.Items.Count - FileList.TopIndex < FileList.Height / FileList.ItemHeight)
            {
                int newIndex = FileList.Items.Count - FileList.Height / FileList.ItemHeight;
                if (newIndex < 0)
                    newIndex = 0;
                FileList.TopIndex = newIndex;
            }
        }

        private void AddFolderButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                AddFiles(System.IO.Directory.EnumerateFiles(dialog.SelectedPath, "*", System.IO.SearchOption.AllDirectories));
            }
        }

        private Point FileListMousePosition = new Point(-1, -1);
        private void FileList_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index > -1)
            {
                BufferedGraphics bufferContext = BufferedGraphicsManager.Current.Allocate(e.Graphics, e.Bounds);

                Brush backBrush;
                Pen borderPen;
                if (FileList.Enabled == false)
                {
                    borderPen = new Pen(Theme.ButtonDisabledBorderColor);
                    backBrush = new SolidBrush(Theme.ButtonDisabledBackgroundColor);
                }
                else if (e.State.HasFlag(DrawItemState.Selected))
                {
                    borderPen = new Pen(Theme.ButtonPressedBorderColor);
                    backBrush = new SolidBrush(Theme.ButtonPressedBackgroundColor);
                }
                else if (e.Bounds.Contains(FileListMousePosition))
                {
                    borderPen = new Pen(Theme.ButtonHoverBorderColor);
                    backBrush = new SolidBrush(Theme.ButtonHoverBackgroundColor);
                }
                else
                {
                    borderPen = null;// new Pen(Theme.ButtonBorderColor);
                    backBrush = new SolidBrush(e.BackColor);// new SolidBrush(Theme.ButtonBackgroundColor);
                }
                if (backBrush != null)
                    bufferContext.Graphics.FillRectangle(backBrush, e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height);
                if (borderPen != null)
                {
                    bufferContext.Graphics.DrawRectangle(borderPen, e.Bounds.X, e.Bounds.Y, e.Bounds.Width - 1, e.Bounds.Height - 1);
                    if (e.State.HasFlag(DrawItemState.Focus) && !e.State.HasFlag(DrawItemState.NoFocusRect))
                        bufferContext.Graphics.DrawRectangle(borderPen, e.Bounds.X + 1, e.Bounds.Y + 1, e.Bounds.Width - 3, e.Bounds.Height - 3);
                }


                backBrush?.Dispose();
                borderPen?.Dispose();

                TextureItem item = FileList.Items[e.Index] as TextureItem;
                SolidBrush filenameBrush = new SolidBrush(Color.FromArgb(220, 220, 220));
                string filenameText = System.IO.Path.GetFileName(item.Filename) + " > " + System.IO.Path.GetFileName(item.OutputFilename);
                using (StringFormat clipFormat = new StringFormat(StringFormatFlags.NoWrap))
                {
                    SizeF availableSize = new SizeF(e.Bounds.Width - 16, e.Bounds.Height);
                    SizeF filenameSize = bufferContext.Graphics.MeasureString(filenameText, e.Font, availableSize, clipFormat, out int charsFitted, out int linesFitted);
                    if (charsFitted < filenameText.Length)
                    {
                        filenameText = filenameText.Substring(0, charsFitted > 3 ? charsFitted - 3 : 0) + "...";
                    }
                    bufferContext.Graphics.DrawString(filenameText, e.Font, filenameBrush, new RectangleF(e.Bounds.X + 2, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height));
                    filenameBrush.Dispose();

                }

                Image stateImage = null;
                if (item.State == TextureItem.ConversionState.Error)
                    stateImage = Properties.Resources.StatusInvalid_16x;
                else if (item.State == TextureItem.ConversionState.Success)
                    stateImage = Properties.Resources.StatusOK_16x;
                else if (item.State == TextureItem.ConversionState.Skip)
                    stateImage = Properties.Resources.StatusAlert_16x;
                else if (item.State == TextureItem.ConversionState.Processing)
                    stateImage = Properties.Resources.Hourglass_16x;

                if (stateImage != null)
                    bufferContext.Graphics.DrawImage(stateImage, e.Bounds.Width - stateImage.Width - 3, e.Bounds.Top + e.Bounds.Height / 2.0f - stateImage.Height / 2.0f, stateImage.Width, stateImage.Height);

                bufferContext.Render();
            }
        }

        private void FileList_MouseMove(object sender, MouseEventArgs e)
        {
            FileListMousePosition = e.Location;
        }

        private void FileList_MouseLeave(object sender, EventArgs e)
        {
            FileListMousePosition = new Point(-1, -1);
        }

        private void FileList_MouseHover(object sender, EventArgs e)
        {
            int hoverIndex = FileList.IndexFromPoint(FileListMousePosition);
            if (hoverIndex > -1)
            {
                TextureItem item = (TextureItem)FileList.Items[hoverIndex];
                string text = "SOURCE: '" + item.Filename + "'\n"
                    + "DESTINATION: '" + item.OutputFilename + "'\n";
                if (item.State != TextureItem.ConversionState.None)
                {
                    text += "STATUS: " + item.State.ToString() + "\n";
                }
                if (!String.IsNullOrEmpty(item.StateMessage))
                {
                    text += item.StateMessage;
                }

                //FileListToolTip.Show(text, FileList, FileListMousePosition);
                FileListToolTip.SetToolTip(FileList, text);
            }
            else
            {
                FileListToolTip.Hide(FileList);
            }
            Win32.ResetMouseHover(FileList.Handle);
        }

        private void FileListToolTip_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            e.DrawBorder();
            e.DrawText(TextFormatFlags.Default);
        }

        private async void ConvertButton_Click(object sender, EventArgs e)
        {
            if (IsConverting && !CancelRemainingConversions)
            {
                CancelRemainingConversions = true;
                ConvertButton.Text = "Skipping Next Textures...";
                ConvertButton.Enabled = false;
            }
            else
            {
                IsConverting = true;
                AddFileButton.Enabled = false;
                AddFolderButton.Enabled = false;
                RemoveFileButton.Enabled = false;
                OptionsPanel.Visible = false;
                ConversionPanel.Visible = true;
                CancelRemainingConversions = false;

                Win32.SetTaskbarState(Handle, Win32.TBPFlag.TBPF_NORMAL);

                int completed = 0;
                ConversionProgressBar.Maximum = FileList.Items.Count;
                foreach (TextureItem tex in FileList.Items)
                {
                    if (CancelRemainingConversions)
                    {
                        tex.State = TextureItem.ConversionState.Skip;
                        tex.StateMessage = "";
                        continue;
                    }
                    else
                    {
                        StatusLabel.Text = "Texture " + (completed + 1) + " / " + FileList.Items.Count;
                        ConvertButton.Text = "Skip Next " + (FileList.Items.Count - completed - 1) + " Textures";
                        ConvertButton.Enabled = completed < FileList.Items.Count - 1;
                        ConversionProgressBar.Value = completed;
                        Win32.SetTaskbarValue(Handle, completed, FileList.Items.Count);
                        CurrentFileLabel.Text = tex.Filename;
                        ConversionOutputTextBox.Text = "";
                        await Texconv.ConvertTexture(tex, ConversionOutputTextBox, CurrentCommandLabel);
                    }
                    FileList.Invalidate(); // The state might have changed
                    completed++;
                }

                Win32.SetTaskbarState(Handle, Win32.TBPFlag.TBPF_NOPROGRESS);

                if (Program.Config.GetValueOrDefault("Conversion", "RemoveSuccessfulFiles", "False").Equals("true", StringComparison.CurrentCultureIgnoreCase))
                {
                    for (int i = FileList.Items.Count - 1; i >= 0; i--)
                    {
                        if ((FileList.Items[i] as TextureItem).State == TextureItem.ConversionState.Success)
                            FileList.Items.RemoveAt(i);
                    }
                }

                AddFileButton.Enabled = true;
                AddFolderButton.Enabled = true;
                RemoveFileButton.Enabled = FileList.SelectedIndex != -1;
                ConvertButton.Enabled = OptionsPanel.Visible = FileList.Items.Count > 0;
                ConvertButton.Text = "Convert " + FileList.Items.Count + " Files";
                ConversionPanel.Visible = false;
                if (FileList.SelectedIndex > -1)
                    LoadOptionsPanel(FileList.SelectedItem as TextureItem);
                IsConverting = false;
            }
        }

        private void InputSRGBCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (FileList.SelectedItem != null)
                (FileList.SelectedItem as TextureItem).IsInputSRGB = InputSRGBCheckBox.Checked;
        }

        private void OutputSRGBCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (FileList.SelectedItem != null)
                (FileList.SelectedItem as TextureItem).IsOutputSRGB = OutputSRGBCheckBox.Checked;

            // Try converting the selected format
            SharpDX.DXGI.Format format = (SharpDX.DXGI.Format)(OutputFormatComboBox.SelectedItem ?? SharpDX.DXGI.Format.Unknown);
            SharpDX.DXGI.Format newSelectedFormat = SharpDX.DXGI.Format.Unknown;
            if (OutputSRGBCheckBox.Checked)
            {
                Enum.TryParse(format.ToString() + "_srgb", true, out newSelectedFormat);
            }
            else if (format.ToString().EndsWith("_srgb", StringComparison.CurrentCultureIgnoreCase))
            {
                Enum.TryParse(format.ToString().Substring(0, format.ToString().Length - 5), true, out newSelectedFormat);
            }

            OutputFormatComboBox.Items.Clear();
            OutputFormatComboBox.Items.AddRange(OutputSRGBCheckBox.Checked ? sRGBFormats.ToArray() : RawFormats.ToArray());
            OutputFormatComboBox.SelectedItem = newSelectedFormat;
        }

        private void OutputFormatComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (FileList.SelectedItem != null)
                (FileList.SelectedItem as TextureItem).OutputFormat = OutputFormatComboBox.SelectedItem as SharpDX.DXGI.Format? ?? SharpDX.DXGI.Format.Unknown;
        }

        private void OutputFilenameTextBox_TextChanged(object sender, EventArgs e)
        {
            if (FileList.SelectedItem != null)
                (FileList.SelectedItem as TextureItem).OutputFilename = OutputFilenameTextBox.Text;
            FileList.Invalidate();
        }

        private void OutputBrowseButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            TextureItem currentItem = FileList.SelectedItem as TextureItem;
            if (currentItem.Filename.EndsWith(".dds", StringComparison.CurrentCultureIgnoreCase))
            {
                dialog.Filter = PNGFilter + "|" + TGAFilter;
            }
            else
            {
                dialog.Filter = DDSFilter;
            }
            dialog.FileName = System.IO.Path.GetFileName(OutputFilenameTextBox.Text);
            dialog.InitialDirectory = System.IO.Path.GetDirectoryName(OutputFilenameTextBox.Text);

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                OutputFilenameTextBox.Text = dialog.FileName;
                FileList.Invalidate();
            }
        }

        private void GenerateMipmapsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (FileList.SelectedItem != null)
                (FileList.SelectedItem as TextureItem).GenerateMipmaps = GenerateMipmapsCheckBox.Checked;
        }

        private void AbortTextureButton_Click(object sender, EventArgs e)
        {
            if (IsConverting)
                Texconv.AbortConversion();
        }

        private void FileList_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Link;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void FileList_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                AddFiles((string[])e.Data.GetData(DataFormats.FileDrop));
            }
        }

        private void AdapterComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Program.Config.SetValue("Conversion", "GPUChoice", AdapterComboBox.SelectedIndex.ToString());
            Program.SaveConfig();
        }

        private void RemoveSuccessfulFilesCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            Program.Config.SetValue("Conversion", "RemoveSuccessfulFiles", RemoveSuccessfulFilesCheckBox.Checked ? "True" : "False");
            Program.SaveConfig();
        }

        private void ResizeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (FileList.SelectedItem != null)
                (FileList.SelectedItem as TextureItem).Resize = ResizeCheckBox.Checked;
            WidthTextBox.Enabled = HeightTextBox.Enabled = ResizeCheckBox.Checked;
        }

        private void WidthTextBox_TextChanged(object sender, EventArgs e)
        {
            if (Int32.TryParse(WidthTextBox.Text, out int width))
                if (FileList.SelectedItem != null)
                    (FileList.SelectedItem as TextureItem).ResizeWidth = width;
        }

        private void HeightTextBox_TextChanged(object sender, EventArgs e)
        {
            if (Int32.TryParse(HeightTextBox.Text, out int height))
                if (FileList.SelectedItem != null)
                    (FileList.SelectedItem as TextureItem).ResizeHeight = height;
        }

        private void Window_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.Config.SetValue("Window", "X", Left.ToString());
            Program.Config.SetValue("Window", "Y", Top.ToString());
            Program.Config.SetValue("Window", "Width", Width.ToString());
            Program.Config.SetValue("Window", "Height", Height.ToString());
            Program.Config.SetValue("Window", "IsMaximized", WindowState == FormWindowState.Maximized ? "True" : "False");
            Program.SaveConfig();
        }

        private void SelectedFileLabel_Click(object sender, EventArgs e)
        {
            if (FileList.SelectedItem != null)
                System.Diagnostics.Process.Start((FileList.SelectedItem as TextureItem).Filename);
        }
    }
}
