﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TexFactory
{
    static class Program
    {
#if DEBUG
        public static string VersionString => System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString() + " DEBUG";
#else
        public static string VersionString => System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
#endif

        private static string ConfigFilename = "TexFactory.ini";
        public static INIFile Config = new INIFile();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Logger.WriteLine("TexFactory v" + VersionString + " starting at " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            // Load config INI
            if (System.IO.File.Exists(ConfigFilename))
                using (System.IO.FileStream stream = new System.IO.FileStream(ConfigFilename, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
                    Config = new INIFile(stream);

            Texconv.EnsureExists();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Window());

            SaveConfig();

            Logger.WriteLine("\n");
            Logger.Dispose();
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logger.WriteLine("[ERROR]: Unhandled exception.\nDetails:\n" + e.ExceptionObject.ToString());
        }

        public static void SaveConfig()
        {
            using (System.IO.FileStream stream = new System.IO.FileStream(ConfigFilename, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.Read))
                Config.Write(stream);
        }
    }
}
