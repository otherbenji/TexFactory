﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace TexFactory.Controls
{
    [System.ComponentModel.DesignerCategory("Code")]
    public class FactoryCheckBox : CheckBox
    {
        private const int BoxSize = 16;
        private const int BoxPadding = 8;

        private bool IsMouseOver = false;
        private int MouseButtonCount = 0;

        public override Size GetPreferredSize(Size proposedSize)
        {
            Graphics g = CreateGraphics();
            SizeF textSize = g.MeasureString(Text, Font);
            g.Dispose();
            return new Size(Padding.Left + Padding.Right + BoxSize + BoxPadding + (int)textSize.Width, Padding.Top + Padding.Bottom + (int)textSize.Height);
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            IsMouseOver = true;
            Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            IsMouseOver = false;
            Invalidate();
        }

        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            base.OnMouseDown(mevent);
            MouseButtonCount++;
            Invalidate();
        }

        protected override void OnMouseUp(MouseEventArgs mevent)
        {
            base.OnMouseUp(mevent);
            MouseButtonCount--;
            Invalidate();
        }

        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Space || keyData == Keys.Enter)
                return true;
            return base.IsInputKey(keyData);
        }

        protected override void OnKeyDown(KeyEventArgs kevent)
        {
            if (kevent.KeyCode == Keys.Space || kevent.KeyCode == Keys.Enter)
            {
                MouseButtonCount++;
                Invalidate();
            }
            base.OnKeyDown(kevent);
        }

        protected override void OnKeyUp(KeyEventArgs kevent)
        {
            if (kevent.KeyCode == Keys.Space || kevent.KeyCode == Keys.Enter)
            {
                MouseButtonCount--;
                Invalidate();
                if (kevent.KeyCode == Keys.Enter)
                    Checked = !Checked;
            }
            base.OnKeyUp(kevent);
        }

        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            MouseButtonCount = 0;
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            pevent.Graphics.Clear(BackColor);

            // Draw checkbox
            Theme.DrawButtonBackground(pevent.Graphics, new Rectangle(Padding.Left, Padding.Top + (Height - Padding.Top - Padding.Bottom) / 2 - BoxSize / 2, BoxSize, BoxSize), IsMouseOver, MouseButtonCount > 0, Enabled, Focused);
            if (Checked)
            {
                Brush fillBrush;
                if (IsMouseOver)
                    fillBrush = new SolidBrush(Theme.ButtonHoverBorderColor);
                else
                    fillBrush = new SolidBrush(Enabled ? ForeColor : Theme.ButtonBorderColor);
                pevent.Graphics.FillRectangle(fillBrush, Padding.Left + 3, Padding.Top + (Height - Padding.Top - Padding.Bottom) / 2 - BoxSize / 2 + 3, BoxSize - 6, BoxSize - 6);
            }

            // Draw text
            if (!String.IsNullOrEmpty(Text))
            {
                SolidBrush textBrush = new SolidBrush(ForeColor);
                Theme.DrawAlignedText(pevent.Graphics, textBrush, Text, Font, new Rectangle(BoxPadding + BoxSize, 0, Width, Height), Padding, TextAlign);
                textBrush.Dispose();
            }
        }
    }
}
