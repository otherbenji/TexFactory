﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace TexFactory.Controls
{
    public class FactoryVScrollBar : Control
    {
        private int _minimum = 0;
        public int Minimum
        {
            get { return _minimum; }
            set
            {
                _minimum = value;
                if (Maximum <= _minimum)
                    Maximum = _minimum + 1;
                if (Value < Minimum)
                    Value = Minimum;
                UpdateThumbButton();
            }
        }

        private int _maximum = 10;
        public int Maximum
        {
            get { return _maximum; }
            set
            {
                _maximum = value;
                if (Minimum >= Maximum)
                    Minimum = Maximum - 1;
                if (Value > Maximum)
                    Value = Maximum;
                UpdateThumbButton();
            }
        }

        private int _value = 0;
        public int Value
        {
            get { return _value; }
            set
            {
                _value = value;
                if (_value < Minimum)
                    _value = Minimum;
                if (_value > Maximum)
                    _value = Maximum;
                UpdateThumbButton();
                Scroll?.Invoke(this, new EventArgs());
            }
        }

        private int _thumbSize = 5;
        public int ThumbSize
        {
            get { return _thumbSize; }
            set
            {
                _thumbSize = value;
                UpdateThumbButton();
            }
        }

        public event EventHandler Scroll;

        private float UnitSize => (Height - 30.0f) / (Maximum - Minimum + ThumbSize);

        private FactoryButton UpButton;
        private FactoryButton ThumbButton;
        private FactoryButton DownButton;
        private bool DraggingThumb = false;
        private Point DragLastPosition;
        private int DragInitialValue;
        private Point DragDelta;

        public FactoryVScrollBar()
        {
            UpButton = new FactoryButton();
            UpButton.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            UpButton.Width = 16;
            UpButton.Height = 16;
            UpButton.MouseMove += (sender, args) => UpButton.BringToFront();
            UpButton.MouseDown += (sender, args) => Value--;
            UpButton.AfterPaint += (sender, args) => args.Graphics.DrawImage(Properties.Resources.UpArrowGlyph, UpButton.Width / 2 - 8, UpButton.Height / 2 - 8, 16, 16);
            Controls.Add(UpButton);

            ThumbButton = new FactoryButton();
            ThumbButton.Height = (Height - 30) / 2;
            ThumbButton.Top = 16;
            ThumbButton.MouseMove += ThumbButton_MouseMove;
            ThumbButton.MouseDown += ThumbButton_MouseDown;
            ThumbButton.MouseUp += (sender, args) => DraggingThumb = args.Button == MouseButtons.Left ? false : DraggingThumb;
            Controls.Add(ThumbButton);

            DownButton = new FactoryButton();
            DownButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            DownButton.Width = 16;
            DownButton.Height = 16;
            DownButton.Top = Height - 16;
            DownButton.MouseMove += (sender, args) => DownButton.BringToFront();
            DownButton.MouseDown += (sender, args) => Value++;
            DownButton.AfterPaint += (sender, args) => args.Graphics.DrawImage(Properties.Resources.DownArrowGlyph, DownButton.Width / 2 - 8, DownButton.Height / 2 - 8, 16, 16);
            Controls.Add(DownButton);

            Resize += FactoryVScrollBar_Resize;
        }

        private void ThumbButton_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                DraggingThumb = true;
                DragLastPosition = ThumbButton.PointToScreen(e.Location);
                DragDelta = new Point();
                DragInitialValue = Value;
            }
        }

        private void ThumbButton_MouseMove(object sender, MouseEventArgs e)
        {
            ThumbButton.BringToFront();
            Point screenLocation = ThumbButton.PointToScreen(e.Location);
            if (DraggingThumb)
            {
                DragDelta = new Point(DragDelta.X + (screenLocation.X - DragLastPosition.X), DragDelta.Y + (screenLocation.Y - DragLastPosition.Y));
                Value = DragInitialValue + (int)Math.Round(DragDelta.Y / UnitSize);
            }
            DragLastPosition = screenLocation;
        }

        private void FactoryVScrollBar_Resize(object sender, EventArgs e)
        {
            UpdateThumbButton();
            UpButton.Width = ThumbButton.Width = DownButton.Width = Width;
        }

        private void UpdateThumbButton()
        {
            int range = Maximum - Minimum + ThumbSize; // the value of the entire thumb track
            float scrollPercent = Value / (float)range;
            
            ThumbButton.Height = (int)Math.Ceiling((Height - 30) * ThumbSize / (float)range);
            ThumbButton.Top = UpButton.Bottom - 1 + (int)(scrollPercent * (Height - 30) * (1.0f - ThumbSize / range));
        }
    }
}
