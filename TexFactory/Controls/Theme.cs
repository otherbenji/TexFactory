﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TexFactory.Controls
{
    public static class Theme
    {
        public static Color ThemeAccentColor => Color.FromArgb(0, 122, 204);

        // Button.Background
        public static Color ButtonBackgroundColor => Color.FromArgb(50, 50, 50);
        public static Color ButtonHoverBackgroundColor => Color.FromArgb(55, 55, 55);
        public static Color ButtonPressedBackgroundColor => ThemeAccentColor;
        public static Color ButtonDisabledBackgroundColor => Color.FromArgb(30, 30, 30);

        // Button.Border
        public static Color ButtonBorderColor => Color.FromArgb(100, 100, 100);
        public static Color ButtonHoverBorderColor => ThemeAccentColor;
        public static Color ButtonPressedBorderColor => ThemeAccentColor;
        public static Color ButtonDisabledBorderColor => ButtonHoverBackgroundColor;

        public static void DrawAlignedText(Graphics g, Brush brush, string text, Font font, Rectangle r, System.Windows.Forms.Padding padding, ContentAlignment align)
        {
            SizeF textSize = g.MeasureString(text, font);
            float tx = 0;
            float ty = 0;
            if (align == ContentAlignment.BottomCenter)
            {
                tx = padding.Left + (r.Width - padding.Left - padding.Right) / 2 - textSize.Width / 2;
                ty = r.Height - padding.Bottom - textSize.Height;
            }
            else if (align == ContentAlignment.BottomLeft)
            {
                tx = padding.Left;
                ty = r.Height - padding.Bottom - textSize.Height;
            }
            else if (align == ContentAlignment.BottomRight)
            {
                tx = r.Width - padding.Right - textSize.Width;
                ty = r.Height - padding.Bottom - textSize.Height;
            }
            else if (align == ContentAlignment.MiddleCenter)
            {
                tx = padding.Left + (r.Width - padding.Left - padding.Right) / 2 - textSize.Width / 2;
                ty = padding.Top + (r.Height - padding.Top - padding.Bottom) / 2 - textSize.Height / 2;
            }
            else if (align == ContentAlignment.MiddleLeft)
            {
                tx = padding.Left;
                ty = padding.Top + (r.Height - padding.Top - padding.Bottom) / 2 - textSize.Height / 2;
            }
            else if (align == ContentAlignment.MiddleRight)
            {
                tx = r.Width - padding.Right - textSize.Width;
                ty = padding.Top + (r.Height - padding.Top - padding.Bottom) / 2 - textSize.Height / 2;
            }
            else if (align == ContentAlignment.TopCenter)
            {
                tx = padding.Left + (r.Width - padding.Left - padding.Right) / 2 - textSize.Width / 2;
                ty = padding.Top;
            }
            else if (align == ContentAlignment.TopLeft)
            {
                tx = padding.Left;
                ty = padding.Top;
            }
            else if (align == ContentAlignment.TopRight)
            {
                tx = r.Width - padding.Right - textSize.Width;
                ty = padding.Top;
            }
            g.DrawString(text, font, brush, r.Left + tx, r.Y + ty);
        }

        public static void DrawButtonBackground(Graphics g, Rectangle r, bool isMouseOver, bool isMousePressed, bool isEnabled, bool isFocused)
        {
            Color backColor;
            Pen borderPen;
            if (isEnabled == false)
            {
                borderPen = new Pen(Theme.ButtonDisabledBorderColor);
                backColor = Theme.ButtonDisabledBackgroundColor;
            }
            else if (isMousePressed)
            {
                borderPen = new Pen(Theme.ButtonPressedBorderColor);
                backColor = Theme.ButtonPressedBackgroundColor;
            }
            else if (isMouseOver)
            {
                borderPen = new Pen(Theme.ButtonHoverBorderColor);
                backColor = Theme.ButtonHoverBackgroundColor;
            }
            else
            {
                borderPen = new Pen(Theme.ButtonBorderColor);
                backColor = Theme.ButtonBackgroundColor;
            }
            SolidBrush backBrush = new SolidBrush(backColor);
            g.FillRectangle(backBrush, r);
            g.DrawRectangle(borderPen, r.X, r.Y, r.Width - 1, r.Height - 1);
            if (isFocused)
            {
                g.DrawRectangle(borderPen, r.X + 1, r.Y + 1, r.Width - 3, r.Height - 3);
            }
            borderPen.Dispose();
            backBrush.Dispose();
        }
    }
}
