﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace TexFactory.Controls
{
    [System.ComponentModel.DesignerCategory("Code")]
    public class FactoryButton : Button
    {
        private bool IsMouseOver = false;
        private int MouseButtonCount = 0;

        public event EventHandler<PaintEventArgs> AfterPaint = null;

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            IsMouseOver = true;
            Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            IsMouseOver = false;
            Invalidate();
        }

        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            base.OnMouseDown(mevent);
            MouseButtonCount++;
            Invalidate();
        }

        protected override void OnMouseUp(MouseEventArgs mevent)
        {
            base.OnMouseUp(mevent);
            MouseButtonCount--;
            Invalidate();
        }

        protected override bool IsInputKey(Keys keyData)
        {
            if (keyData == Keys.Space || keyData == Keys.Enter)
                return true;
            return base.IsInputKey(keyData);
        }

        protected override void OnKeyDown(KeyEventArgs kevent)
        {
            if (kevent.KeyCode == Keys.Space || kevent.KeyCode == Keys.Enter)
            {
                MouseButtonCount++;
                Invalidate();
            }
            base.OnKeyDown(kevent);
        }

        protected override void OnKeyUp(KeyEventArgs kevent)
        {
            if (kevent.KeyCode == Keys.Space || kevent.KeyCode == Keys.Enter)
            {
                MouseButtonCount--;
                Invalidate();
                if (kevent.KeyCode == Keys.Enter)
                    PerformClick();
            }
            base.OnKeyUp(kevent);
        }

        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            MouseButtonCount = 0;
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            Theme.DrawButtonBackground(pevent.Graphics, new Rectangle(0, 0, Width, Height), IsMouseOver, MouseButtonCount > 0, Enabled, Focused);
            if (!String.IsNullOrEmpty(Text))
            {
                SolidBrush textBrush = new SolidBrush(ForeColor);
                Theme.DrawAlignedText(pevent.Graphics, textBrush, Text, Font, new Rectangle(0, 0, Width, Height), Padding, TextAlign);
                textBrush.Dispose();
            }
            AfterPaint?.Invoke(this, pevent);
        }
    }
}
