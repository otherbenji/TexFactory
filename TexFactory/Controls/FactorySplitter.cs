﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace TexFactory.Controls
{
    [System.ComponentModel.DesignerCategory("Code")]
    public class FactorySplitter : Splitter
    {
        private bool IsMouseOver = false;
        private bool IsMousePressed = false;
        private int MouseButtonCount = 0;
        private bool IsSplitting = false;

        private static System.Reflection.MethodInfo ApplySplitPosition = typeof(Splitter).GetMethod("ApplySplitPosition", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            IsMouseOver = true;
            Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            IsMouseOver = false;
            Invalidate();
        }

        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            base.OnMouseDown(mevent);
            if (mevent.Button == MouseButtons.Left)
                IsSplitting = true;
            IsMousePressed = true;
            MouseButtonCount++;
            Invalidate();
        }

        protected override void OnMouseUp(MouseEventArgs mevent)
        {
            IsSplitting = false;
            MouseButtonCount--;
            if (MouseButtonCount == 0)
                IsMousePressed = false;
            Invalidate();
        }

        private bool IsInMouseMove = false;
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (IsInMouseMove)
                return;

            IsInMouseMove = true;
            base.OnMouseMove(e);
            if (IsSplitting)
            {
                ApplySplitPosition.Invoke(this, new object[] { });
            }
            IsInMouseMove = false;
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle paintRectangle;
            if (this.Dock == DockStyle.Left || this.Dock == DockStyle.Right)
            {
                paintRectangle = new Rectangle(0, -1, Width, Height + 2);
            }
            else
            {
                paintRectangle = new Rectangle(-1, 0, Width + 2, Height);
            }
            if (IsMouseOver || IsMousePressed)
                Theme.DrawButtonBackground(e.Graphics, paintRectangle, IsMouseOver, IsMousePressed, Enabled, Focused);
        }

        protected override void OnSplitterMoved(SplitterEventArgs sevent)
        {
            base.OnSplitterMoved(sevent);
            Invalidate();
        }

        protected override void OnSplitterMoving(SplitterEventArgs sevent)
        {
            base.OnSplitterMoving(sevent);
            Invalidate();
        }
    }
}
