﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace TexFactory.Controls
{
    [System.ComponentModel.DesignerCategory("Code")]
    public class FactoryDropDown : Control
    {
        private class StylishForm : Form
        {
            public StylishForm()
            {
                FormBorderStyle = FormBorderStyle.None;
                DoubleBuffered = true;
                StartPosition = FormStartPosition.Manual;
                //CreateParams.ExStyle |= Win32.WS_EX_NOACTIVATE;
            }

            protected override bool IsInputKey(Keys keyData)
            {
                if (keyData.HasFlag(Keys.Up) || keyData.HasFlag(Keys.Down))
                    return true;
                return base.IsInputKey(keyData);
            }

            protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
            {
                if (keyData == Keys.Up || keyData == Keys.Down ||
                    keyData == Keys.Left || keyData == Keys.Right)
                {
                    object sender = Control.FromHandle(msg.HWnd);
                    KeyEventArgs e = new KeyEventArgs(keyData);
                    if (msg.Msg == Win32.WM_KEYDOWN)
                    {
                        OnKeyDown(e);
                    }
                    else if (msg.Msg == Win32.WM_KEYUP)
                    {
                        OnKeyUp(e);
                    }
                    return true;
                }

                return base.ProcessCmdKey(ref msg, keyData);
            }

            /*protected override void WndProc(ref Message m)
            {
                if (m.Msg == Win32.WM_MOUSEACTIVATE)
                {
                    m.Result = (IntPtr)Win32.MouseActivate.MA_NOACTIVATE;
                }
                else
                {
                    base.WndProc(ref m);
                }
            }*/
        }

        public System.Collections.ObjectModel.ObservableCollection<object> Items { get; } = new System.Collections.ObjectModel.ObservableCollection<object>();

        private int _selectedIndex = -1;
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                _selectedIndex = value;
                if (_selectedIndex >= Items.Count)
                    _selectedIndex = Items.Count - 1;
                DropDownButton.Text = SelectedItem?.ToString() ?? "";
                OnSelectedItemChanged(this, new EventArgs());
            }
        }

        public object SelectedItem
        {
            get { return _selectedIndex > -1 ? Items[_selectedIndex] : null; }
            set
            {
                int index = Items.IndexOf(value);
                if (index != _selectedIndex)
                {
                    SelectedIndex = index;
                }
            }
        }

        public event EventHandler SelectedIndexChanged;
        public event EventHandler SelectedItemChanged;

        private FactoryButton DropDownButton = null;
        private StylishForm DropDownWindow = null;
        private FactoryVScrollBar DropDownScrollBar = null;
        private int MenuMouseCount = 0;
        private int MenuMouseIndex = -1;
        private int KeyboardFocusIndex = -1;
        private bool KeyboardSelectDown = false;
        private bool JustHidden = false;

        public FactoryDropDown()
        {
            Items.CollectionChanged += Items_CollectionChanged;
            SizeChanged += FactoryDropDown_SizeChanged;

            DropDownButton = new FactoryButton();
            DropDownButton.Dock = DockStyle.Fill;
            DropDownButton.TextAlign = ContentAlignment.MiddleLeft;
            DropDownButton.Padding = new Padding(2);
            Controls.Add(DropDownButton);
            DropDownButton.AfterPaint += DropDownButton_AfterPaint;
            DropDownButton.MouseUp += DropDownButton_MouseUp;
            DropDownButton.KeyUp += DropDownButton_KeyUp;

            DropDownWindow = new StylishForm();
            DropDownWindow.BackColor = Theme.ButtonBackgroundColor;
            DropDownWindow.ShowInTaskbar = false;
            DropDownWindow.KeyPreview = true;
            //DropDownWindow.LostFocus += DropDownWindow_LostFocus;
            DropDownWindow.Paint += DropDownWindow_Paint;
            DropDownWindow.MouseDown += DropDownWindow_MouseDown;
            DropDownWindow.MouseUp += DropDownWindow_MouseUp;
            DropDownWindow.MouseLeave += DropDownWindow_MouseLeave;
            DropDownWindow.MouseMove += DropDownWindow_MouseMove;
            DropDownWindow.SizeChanged += DropDownWindow_SizeChanged;
            DropDownWindow.MouseWheel += DropDownWindow_MouseWheel;
            DropDownWindow.KeyDown += DropDownWindow_KeyDown;
            DropDownWindow.KeyUp += DropDownWindow_KeyUp;

            DropDownScrollBar = new FactoryVScrollBar();
            DropDownScrollBar.Width = 16;
            DropDownScrollBar.Height = DropDownWindow.Height;
            DropDownScrollBar.Left = DropDownWindow.Width - DropDownScrollBar.Width - 1;
            DropDownScrollBar.Scroll += (sender, args) => DropDownWindow.Invalidate();
            DropDownWindow.Controls.Add(DropDownScrollBar);

            RecursiveAddFocusHandler(DropDownWindow);
        }

        private void DropDownWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Space || e.KeyCode == Keys.Enter) && KeyboardSelectDown)
            {
                SelectedIndex = KeyboardFocusIndex;
                DropDownWindow.Hide();
                DropDownButton.Focus();
                JustHidden = false;
                KeyboardSelectDown = false;
            }
        }

        private void DropDownWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space || e.KeyCode == Keys.Enter)
            {
                KeyboardSelectDown = true;
                DropDownWindow.Invalidate();
            }
            else if (e.KeyCode == Keys.Up)
            {
                KeyboardFocusIndex -= 1;
                if (KeyboardFocusIndex < 0)
                    KeyboardFocusIndex = 0;
                if (KeyboardFocusIndex < DropDownScrollBar.Value)
                    DropDownScrollBar.Value = KeyboardFocusIndex;
                DropDownWindow.Invalidate();
            }
            else if (e.KeyCode == Keys.Down)
            {
                KeyboardFocusIndex += 1;
                if (KeyboardFocusIndex >= Items.Count)
                    KeyboardFocusIndex = Items.Count - 1;
                if (KeyboardFocusIndex >= DropDownScrollBar.Value + DropDownScrollBar.ThumbSize)
                    DropDownScrollBar.Value = KeyboardFocusIndex - DropDownScrollBar.ThumbSize + 1;
                DropDownWindow.Invalidate();
            }
        }

        private void DropDownButton_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space || e.KeyCode == Keys.Enter)
            {
                ShowDropDown();
                if (KeyboardFocusIndex > -1)
                {
                    if (KeyboardFocusIndex < DropDownScrollBar.Value)
                        DropDownScrollBar.Value = KeyboardFocusIndex;
                    if (KeyboardFocusIndex >= DropDownScrollBar.Value + DropDownScrollBar.ThumbSize)
                        DropDownScrollBar.Value = KeyboardFocusIndex - DropDownScrollBar.ThumbSize + 1;
                }
            }
        }

        private void DropDownWindow_MouseWheel(object sender, MouseEventArgs e)
        {
            if (DropDownScrollBar.Enabled)
                DropDownScrollBar.Value -= e.Delta / 120;
        }

        private void DropDownWindow_SizeChanged(object sender, EventArgs e)
        {
            DropDownScrollBar.Left = DropDownWindow.Width - (DropDownScrollBar.Enabled ? DropDownScrollBar.Width : 0) - 1;
            DropDownScrollBar.Height = DropDownWindow.Height - 1;
        }

        private void RecursiveAddFocusHandler(Control control)
        {
            control.LostFocus += DropDownWindow_LostFocus;
            foreach (Control child in control.Controls)
                RecursiveAddFocusHandler(child);
        }

        private bool RecursiveIsFocused(Control control)
        {
            if (control.Focused)
                return true;

            foreach (Control child in control.Controls)
                if (RecursiveIsFocused(child))
                    return true;

            return false;
        }

        private void DropDownWindow_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.X >= 0 && e.X < DropDownWindow.Width - (DropDownScrollBar.Enabled ? DropDownScrollBar.Width : 0) && e.Y >= 0 && e.Y < DropDownWindow.Height)
                MenuMouseIndex = (e.Y / Height) + DropDownScrollBar.Value;
            else
                MenuMouseIndex = -1;

            DropDownWindow.Invalidate();
        }

        private void DropDownWindow_MouseLeave(object sender, EventArgs e)
        {
            MenuMouseIndex = -1;

            DropDownWindow.Invalidate();
        }

        private void DropDownWindow_MouseUp(object sender, MouseEventArgs e)
        {
            MenuMouseCount--;

            DropDownWindow.Invalidate();

            if (e.X >= 0 && e.X < DropDownWindow.Width - (DropDownScrollBar.Enabled ? DropDownScrollBar.Width : 0) && e.Y >= 0 && e.Y < DropDownWindow.Height)
            {
                SelectedIndex = (e.Y / Height) + DropDownScrollBar.Value;
                DropDownWindow.Hide();
                DropDownButton.Focus();
            }
        }

        private void DropDownWindow_MouseDown(object sender, MouseEventArgs e)
        {
            MenuMouseCount++;

            if (e.X >= 0 && e.X < DropDownWindow.Width - (DropDownScrollBar.Enabled ? DropDownScrollBar.Width : 0) && e.Y >= 0 && e.Y < DropDownWindow.Height)
            {
                KeyboardFocusIndex = (e.Y / Height) - DropDownScrollBar.Value;
            }

            DropDownWindow.Invalidate();
        }

        private void FactoryDropDown_SizeChanged(object sender, EventArgs e)
        {
            DropDownWindow.Width = Width;
            Graphics g = CreateGraphics();
            foreach (object item in Items)
            {
                if (!String.IsNullOrEmpty(item.ToString()))
                {
                    SizeF textSize = g.MeasureString(item.ToString(), Font);
                    if (DropDownWindow.Width < textSize.Width + 4)
                        DropDownWindow.Width = (int)textSize.Width + 4;
                }
            }
            g.Dispose();
        }

        private void DropDownWindow_Paint(object sender, PaintEventArgs e)
        {
            int top = 0;
            Brush backBrush;
            Pen borderPen = new Pen(Theme.ButtonBorderColor);
            e.Graphics.DrawRectangle(borderPen, 0, -1, DropDownWindow.Width - 1, DropDownWindow.Height);
            borderPen.Dispose();
            borderPen = null;

            int startIndex = DropDownScrollBar.Value;
            for (int i = startIndex; i < Items.Count; i++)
            {
                object item = Items[i];
                top = (i - startIndex) * Height;
                if (Enabled == false)
                {
                    borderPen = new Pen(Theme.ButtonDisabledBorderColor);
                    backBrush = new SolidBrush(Theme.ButtonDisabledBackgroundColor);
                }
                else if (i == SelectedIndex)
                {
                    borderPen = new Pen(Theme.ButtonPressedBorderColor);
                    backBrush = new SolidBrush(Theme.ButtonHoverBackgroundColor);
                }
                else if (i == MenuMouseIndex)
                {
                    if (MenuMouseCount > 0)
                    {
                        borderPen = new Pen(Theme.ButtonPressedBorderColor);
                        backBrush = new SolidBrush(Theme.ButtonPressedBackgroundColor);
                    }
                    else
                    {
                        borderPen = new Pen(Theme.ButtonHoverBorderColor);
                        backBrush = new SolidBrush(Theme.ButtonHoverBackgroundColor);
                    }
                }
                else
                {
                    borderPen = i == KeyboardFocusIndex ? new Pen(Theme.ButtonBorderColor) : null;// new Pen(Theme.ButtonBorderColor);
                    backBrush = null;// new SolidBrush(Theme.ButtonBackgroundColor);
                }

                if (i == KeyboardFocusIndex && KeyboardSelectDown)
                {
                    backBrush?.Dispose();
                    backBrush = new SolidBrush(Theme.ButtonPressedBackgroundColor);
                    borderPen?.Dispose();
                    borderPen = new Pen(Theme.ButtonPressedBorderColor);
                }

                if (backBrush != null)
                    e.Graphics.FillRectangle(backBrush, 0, top, DropDownWindow.Width - (DropDownScrollBar.Enabled ? DropDownScrollBar.Width + 1 : 0), Height);
                if (borderPen != null)
                {
                    e.Graphics.DrawRectangle(borderPen, 0, top, DropDownWindow.Width - (DropDownScrollBar.Enabled ? DropDownScrollBar.Width + 1 : 0) - 1, Height - 1);
                    if (i == KeyboardFocusIndex)
                        e.Graphics.DrawRectangle(borderPen, 1, top + 1, DropDownWindow.Width - (DropDownScrollBar.Enabled ? DropDownScrollBar.Width + 1 : 0) - 3, Height - 3);
                }
                if (!String.IsNullOrEmpty(item.ToString()))
                {
                    Theme.DrawAlignedText(e.Graphics, Brushes.Gainsboro, item.ToString(), Font, new Rectangle(0, top, DropDownWindow.Width - DropDownScrollBar.Width - 1, Height), new Padding(2), ContentAlignment.MiddleLeft);
                }
                backBrush?.Dispose();
                backBrush = null;
                borderPen?.Dispose();
                borderPen = null;
            }
        }

        private void DropDownWindow_LostFocus(object sender, EventArgs e)
        {
            // Check if the focus went elsewhere in the DropDownWindow
            if (RecursiveIsFocused(DropDownWindow))
                return;

            Rectangle buttonScreen = DropDownButton.RectangleToScreen(new Rectangle(0, 0, DropDownButton.Width, DropDownButton.Height));
            if (buttonScreen.Contains(Control.MousePosition))
                JustHidden = true;
            else
                JustHidden = false;
            DropDownWindow.Hide();
        }

        private void DropDownButton_MouseUp(object sender, EventArgs e)
        {
            if (JustHidden)
            {
                JustHidden = false;
            }
            else
            {
                ShowDropDown();
            }
        }

        private void DropDownButton_AfterPaint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(Properties.Resources.DownArrowGlyph, DropDownButton.Width - 8 - 16, DropDownButton.Height / 2 - 8, 16, 16);
        }

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add && e.NewItems.Count == Items.Count && SelectedIndex == -1)
            {
                _selectedIndex = 0;
                DropDownButton.Text = SelectedItem?.ToString() ?? "";
            }
            OnSizeChanged(new EventArgs());
        }

        protected virtual void OnSelectedItemChanged(object sender, EventArgs e)
        {
            SelectedIndexChanged?.Invoke(sender, e);
            SelectedItemChanged?.Invoke(sender, e);
        }

        public void ShowDropDown()
        {
            KeyboardFocusIndex = SelectedIndex;

            Point bottomScreenPos = PointToScreen(new Point(0, Height));
            Rectangle screenBounds = Screen.FromControl(this).WorkingArea;
            int maxShowableCount = (int)Math.Floor((screenBounds.Bottom - (double)bottomScreenPos.Y) / Height);
            int maxCount = maxShowableCount < Items.Count ? maxShowableCount : Items.Count;
            DropDownWindow.Height = maxCount * Height;
            if (Items.Count > maxCount)
            {
                DropDownScrollBar.Maximum = Items.Count - maxCount;
                DropDownScrollBar.ThumbSize = maxCount;
                DropDownScrollBar.Visible = true;
                DropDownScrollBar.Enabled = true;
            }
            else
            {
                DropDownScrollBar.Visible = false;
                DropDownScrollBar.Enabled = false;
                DropDownScrollBar.Value = 0;
            }

            DropDownWindow.Left = bottomScreenPos.X;
            DropDownWindow.Top = bottomScreenPos.Y;
            //Win32.ShowWindow(DropDownWindow.Handle, Win32.ShowWindowCommands.Show);

            //Win32.AnimateWindow(DropDownWindow.Handle, 200, Win32.AnimateWindowFlags.AW_SLIDE | Win32.AnimateWindowFlags.AW_VER_POSITIVE | Win32.AnimateWindowFlags.AW_ACTIVATE);
            DropDownWindow.Show();
            DropDownWindow_SizeChanged(this, new EventArgs());

            //DropDownWindow.Focus();
            //Win32.SetFocus(DropDownWindow.Handle);
        }
    }
}
