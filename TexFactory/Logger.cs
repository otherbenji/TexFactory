﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TexFactory
{
    public class Logger
    {
        private const string LogFilename = "TexFactory Log.txt";

        private static StreamWriter Writer = null;

        public static void WriteLine(string line)
        {
            if (Writer == null)
            {
                try
                {
                    FileStream stream = new FileStream(LogFilename, FileMode.Append, FileAccess.Write, FileShare.Read);
                    Writer = new StreamWriter(stream, Encoding.UTF8);
                }
                catch (Exception ex)
                {
                    if (System.Windows.Forms.MessageBox.Show("ERROR: Could not open the log file.\nException details:\n\n" + ex.ToString() + "\n\nCopy message to clipboard?", "TexFactory Error", System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        System.Windows.Forms.Clipboard.SetText(ex.ToString());
                    }
                }
            }
            Writer?.WriteLine(line);
            Writer.Flush();
        }

        public static void Dispose()
        {
            Writer?.Dispose();
            Writer = null;
        }
    }
}
