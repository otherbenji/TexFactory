﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TexFactory
{
    public class INIFile
    {
        private class Section
        {
            public string Name { get; }
            public Dictionary<string, string> Values { get; } = new Dictionary<string, string>();

            public Section(string name)
            {
                Name = name;
            }
        }

        private Dictionary<string, Section> Sections { get; } = new Dictionary<string, Section>();

        public INIFile()
        {

        }

        public INIFile(System.IO.Stream stream)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(stream))
            {
                Section currentSection = new Section("");
                Sections.Add("", currentSection);
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine().Trim();

                    // Ignore blank lines
                    if (line.Length == 0)
                        continue;

                    // Ignore comments
                    if (line.StartsWith(";"))
                        continue;

                    // Check if the line is a section header
                    if (line.StartsWith("["))
                    {
                        string sectionName = line.Substring(1, line.Length - 2);
                        currentSection = new Section(sectionName);
                        Sections.Add(sectionName, currentSection);
                    }
                    else
                    {
                        int equalsIndex = line.IndexOf("=");
                        if (equalsIndex == -1)
                        {
                            Logger.WriteLine("Invalid INI Line: \"" + line + "\"!");
                            continue;
                        }
                        string key = line.Substring(0, equalsIndex);
                        string value = line.Substring(equalsIndex + 1);
                        if (currentSection.Values.ContainsKey(key))
                        {
                            currentSection.Values[key] = value;
                        }
                        else
                        {
                            currentSection.Values.Add(key, value);
                        }
                    }
                }
            }
        }

        public void SetValue(string section, string key, string value)
        {
            if (!Sections.ContainsKey(section))
            {
                Sections.Add(section, new Section(section));
            }
            if (!Sections[section].Values.ContainsKey(key))
            {
                Sections[section].Values.Add(key, value);
            }
            else
            {
                Sections[section].Values[key] = value;
            }
        }

        public string GetValueOrDefault(string section, string key, string defaultValue = null)
        {
            if (Sections.ContainsKey(section))
            {
                if (Sections[section].Values.ContainsKey(key))
                {
                    return Sections[section].Values[key];
                }
                else
                {
                    return defaultValue;
                }
            }
            else
            {
                return defaultValue;
            }
        }

        public void Write(System.IO.Stream stream)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(stream))
            {
                foreach (Section section in Sections.Values)
                {
                    if (section.Name.Length > 0)
                        writer.WriteLine("[" + section.Name + "]");
                    foreach (KeyValuePair<string, string> entry in section.Values)
                    {
                        writer.WriteLine(entry.Key + "=" + entry.Value);
                    }
                    writer.WriteLine();
                }
            }
        }
    }
}
