﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace TexFactory
{
    public static class Texconv
    {
        private const string ExecutableFilename = @"bin/texconv.exe";
        private static string ApplicationDirectory => System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

        // Global state for aborting: hacky but works
        private static Process CurrentTexconv = null;
        private static TextureItem CurrentTexture = null;

        public static void EnsureExists()
        {
            try
            {
                Directory.CreateDirectory(Path.Combine(ApplicationDirectory, Path.GetDirectoryName(ExecutableFilename)));
                if (!File.Exists(Path.Combine(ApplicationDirectory, ExecutableFilename)))
                    File.WriteAllBytes(Path.Combine(ApplicationDirectory, ExecutableFilename), Properties.Resources.TexconvBinary);
            }
            catch (IOException ex)
            {
                Logger.WriteLine("[ERROR]: Could not extract texconv binary.\nDetails:\n" + ex.ToString());
                throw ex;
            }
        }

        public static void AbortConversion()
        {
            if (CurrentTexconv != null)
            {
                try
                {
                    CurrentTexconv.Kill();
                    if (CurrentTexture.State == TextureItem.ConversionState.Processing)
                    {
                        CurrentTexture.State = TextureItem.ConversionState.Skip;
                        CurrentTexture.StateMessage = "";
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLine("[ERROR]: Couldn't kill texconv.\nDetails:\n" + ex.ToString());
                }
            }
        }

        public static async Task ConvertTexture(TextureItem texture, System.Windows.Forms.TextBox outputTextBox, System.Windows.Forms.Label commandLabel)
        {
            texture.State = TextureItem.ConversionState.Processing;
            CurrentTexture = texture;

            // Convert to a random temp file, then copy on success. Along with being more resistant to failure, this also allows us to rename the output.
            string RandomFilename = Guid.NewGuid().ToString();
            string tempSrcName = Path.Combine(Path.GetTempPath(), RandomFilename + Path.GetExtension(texture.Filename));
            string tempDestName = Path.ChangeExtension(tempSrcName, Path.GetExtension(texture.OutputFilename));

            Process texconv = new Process();
            texconv.StartInfo.FileName = ExecutableFilename;
            int adapterSelection = 1;
            try
            {
                adapterSelection = Int32.Parse(Program.Config.GetValueOrDefault("Conversion", "GPUChoice", "1"));
            }
            catch
            {
                Logger.WriteLine("[WARNING]: Couldn't parse preferred adapter index from '" + Program.Config.GetValueOrDefault("Conversion", "GPUChoice", "1") + "'!");
            }
            if (adapterSelection == 0)
                texconv.StartInfo.Arguments += " -nogpu";
            else if (adapterSelection > 1)
                texconv.StartInfo.Arguments += " -gpu " + (adapterSelection - 1);
            if (texture.Resize)
                texconv.StartInfo.Arguments += " -w " + texture.ResizeWidth + " -h " + texture.ResizeHeight;
            texconv.StartInfo.Arguments += " -y -sepalpha -ft " + Path.GetExtension(texture.OutputFilename).Substring(1).ToUpper();
            if (!texture.GenerateMipmaps)
                texconv.StartInfo.Arguments += " -m 1";
            texconv.StartInfo.Arguments += " -o \"" + Path.GetDirectoryName(tempDestName) + "\"";

            // sRGB Handling
            if (texture.IsInputSRGB)
            {
                if (texture.IsOutputSRGB)
                    texconv.StartInfo.Arguments += " -srgb";
                else
                    texconv.StartInfo.Arguments += " -srgbi";
            }
            else
            {
                if (texture.IsOutputSRGB)
                    texconv.StartInfo.Arguments += " -srgbo";
            }
            
            //if (texture.OutputFilename.EndsWith(".dds", StringComparison.CurrentCultureIgnoreCase))
            //{
                if (texture.OutputFormat != SharpDX.DXGI.Format.Unknown)
                {
                    texconv.StartInfo.Arguments += " -f " + texture.OutputFormat.ToString();
                }
            //}
            /*else
            {
                // HACK: Convert two-channel RG to full RGBA
                if (texture.OutputFormat >= SharpDX.DXGI.Format.R8G8_Typeless && texture.OutputFormat <= SharpDX.DXGI.Format.R8G8_SInt)
                {
                    texconv.StartInfo.Arguments += " -f " + SharpDX.DXGI.Format.B8G8R8A8_UNorm.ToString();
                }
            }*/
            texconv.StartInfo.Arguments += " \"" + tempSrcName + "\"";

            texconv.OutputDataReceived += (sender, args) =>
            {
                if (args.Data == null)
                    return;

                if (args.Data.Contains("FAILED"))
                {
                    texture.State = TextureItem.ConversionState.Error;
                    texture.StateMessage = args.Data;
                }

                outputTextBox.FindForm().Invoke(new Action(() =>
                {
                    outputTextBox.AppendText(args.Data + "\n");
                }));
            };
            texconv.StartInfo.UseShellExecute = false;
            texconv.StartInfo.RedirectStandardOutput = true;
            texconv.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            texconv.StartInfo.CreateNoWindow = true;

            commandLabel.Text = "";
            await Task.Run(() => File.Copy(texture.Filename, tempSrcName));
            commandLabel.Text = texconv.StartInfo.FileName + " " + texconv.StartInfo.Arguments;
            CurrentTexconv = texconv;
            await Task.Run(() =>
            {
                //Logger.WriteLine("[INFO]: Converting " + texture.Filename);

                System.Threading.Thread.Sleep(1000);
                texconv.Start();
                texconv.BeginOutputReadLine();

                texconv.WaitForExit();

                //Logger.WriteLine("[INFO]: Conversion finished.");
                File.Delete(tempSrcName);
            });
            CurrentTexconv = null;

            if (File.Exists(tempDestName))
            {
                try
                {
                    if (File.Exists(texture.OutputFilename))
                        File.Delete(texture.OutputFilename);
                    File.Move(tempDestName, texture.OutputFilename);
                    texture.StoreResultingMetadata();
                    texture.State = TextureItem.ConversionState.Success;
                    texture.StateMessage = "";
                }
                catch (Exception ex)
                {
                    Logger.WriteLine("[ERROR]: Couldn't finalize.\nDetails:\n" + ex.ToString());
                    texture.State = TextureItem.ConversionState.Error;
                    texture.StateMessage = "Failed to finalize. Details:\n" + ex.Message;
                }
            }
            else if (texture.State == TextureItem.ConversionState.Processing)
            {
                texture.State = TextureItem.ConversionState.Error;
                texture.StateMessage = "Failed to complete processing.";
            }

            Logger.WriteLine("[INFO]: Finished converting '" + texture.Filename + "' to '" + texture.OutputFilename + "'. State: " + texture.State.ToString() + ", StateMessage: " + texture.StateMessage);
            CurrentTexture = null;
        }
    }
}
