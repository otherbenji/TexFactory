﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using D3D11 = SharpDX.Direct3D11;

namespace TexFactory
{
    public static class DDSFile
    {
        public enum DDSCaps
        {
            Complex = 0x08,
            MipMap = 0x400000,
            Texture = 0x1000
        }

        public enum DDSCaps2
        {
            CubeMap = 0x200,
            CubeMapPositiveX = 0x400,
            CubeMapNegativeX = 0x800,
            CubeMapPositiveY = 0x1000,
            CubeMapNegativeY = 0x2000,
            CubeMapPositiveZ = 0x4000,
            CubeMapNegativeZ = 0x8000,
            Volume = 0x200000,

            CubeMapAllFaces = CubeMapPositiveX | CubeMapPositiveY | CubeMapPositiveZ | CubeMapNegativeX | CubeMapNegativeY | CubeMapNegativeZ
        }

        public enum DDSFlags
        {
            Caps = 0x01,
            Height = 0x02,
            Width = 0x04,
            Pitch = 0x08,
            PixelFormat = 0x1000,
            MipMapCount = 0x20000,
            LinearSize = 0x80000,
            Depth = 0x800000,

            Required = Caps | Height | Width | PixelFormat
        }

        public enum DDSPFFlags
        {
            AlphaPixels = 0x01,
            Alpha = 0x02,
            FourCC = 0x04,
            RGB = 0x40,
            YUV = 0x200,
            Luminance = 0x20000
        }

        public struct DDSHeaderDX10
        {
            public SharpDX.DXGI.Format dxgiFormat;
            public D3D11.ResourceDimension resourceDimension;
            public uint miscFlag;
            public uint arraySize;
            public uint miscFlags2;
        }

        public struct DDSPixelFormat
        {
            public int dwSize;
            public DDSPFFlags dwFlags;
            public int dwFourCC;
            public int dwRGBBitCount;
            public uint dwRBitMask;
            public uint dwGBitMask;
            public uint dwBBitMask;
            public uint dwABitMask;
        }

        public class DDSHeader
        {
            public int dwMagic;
            public int dwSize;
            public DDSFlags dwFlags;
            public int dwHeight;
            public int dwWidth;
            public int dwPitchOrLinearSize;
            public int dwDepth;
            public int dwMipMapCount;
            public int[] dwReserved1;
            public DDSPixelFormat ddspf;
            public DDSCaps dwCaps;
            public DDSCaps2 dwCaps2;
            public int dwCaps3;
            public int dwCaps4;
            public int dwReserved2;

            public bool HasExtendedHeader;
            public DDSHeaderDX10 ExtendedHeader;

            public DDSHeader()
            {
                dwMagic = 0x20534444;
                dwSize = 0x7C;
                dwFlags = DDSFlags.Required;
                dwDepth = 1;
                dwCaps = DDSCaps.Texture;
                dwCaps2 = 0;
                dwReserved1 = new int[11];
                ddspf.dwSize = 0x20;
                ddspf.dwFlags = DDSPFFlags.FourCC;
                HasExtendedHeader = false;
            }

            public void Write(System.IO.BinaryWriter writer)
            {
                writer.Write(dwMagic);
                writer.Write(dwSize);
                writer.Write((int)dwFlags);
                writer.Write(dwHeight);
                writer.Write(dwWidth);
                writer.Write(dwPitchOrLinearSize);
                writer.Write(dwDepth);
                writer.Write(dwMipMapCount);
                for (int i = 0; i < 11; i++) writer.Write(dwReserved1[i]);
                writer.Write(ddspf.dwSize);
                writer.Write((int)ddspf.dwFlags);
                writer.Write(ddspf.dwFourCC);
                writer.Write(ddspf.dwRGBBitCount);
                writer.Write(ddspf.dwRBitMask);
                writer.Write(ddspf.dwGBitMask);
                writer.Write(ddspf.dwBBitMask);
                writer.Write(ddspf.dwABitMask);
                writer.Write((int)dwCaps);
                writer.Write((int)dwCaps2);
                writer.Write(dwCaps3);
                writer.Write(dwCaps4);
                writer.Write(dwReserved2);

                if (HasExtendedHeader)
                {
                    writer.Write((uint)ExtendedHeader.dxgiFormat);
                    writer.Write((uint)ExtendedHeader.resourceDimension);
                    writer.Write(ExtendedHeader.miscFlag);
                    writer.Write(ExtendedHeader.arraySize);
                    writer.Write(ExtendedHeader.miscFlags2);
                }
            }

            public bool Read(System.IO.BinaryReader reader)
            {
                dwMagic = reader.ReadInt32();
                if (dwMagic != 0x20534444)
                    return false;

                dwSize = reader.ReadInt32();
                if (dwSize != 0x7C)
                    return false;

                dwFlags = (DDSFlags)reader.ReadInt32();
                dwHeight = reader.ReadInt32();
                dwWidth = reader.ReadInt32();
                dwPitchOrLinearSize = reader.ReadInt32();
                dwDepth = reader.ReadInt32();
                dwReserved1 = new int[11];
                dwMipMapCount = reader.ReadInt32();
                for (int i = 0; i < 11; i++)
                    dwReserved1[i] = reader.ReadInt32();
                ddspf.dwSize = reader.ReadInt32();
                ddspf.dwFlags = (DDSPFFlags)reader.ReadInt32();
                ddspf.dwFourCC = reader.ReadInt32();
                ddspf.dwRGBBitCount = reader.ReadInt32();
                ddspf.dwRBitMask = reader.ReadUInt32();
                ddspf.dwGBitMask = reader.ReadUInt32();
                ddspf.dwBBitMask = reader.ReadUInt32();
                ddspf.dwABitMask = reader.ReadUInt32();
                dwCaps = (DDSCaps)reader.ReadInt32();
                dwCaps2 = (DDSCaps2)reader.ReadInt32();
                dwCaps3 = reader.ReadInt32();
                dwCaps4 = reader.ReadInt32();
                dwReserved2 = reader.ReadInt32();

                if (ddspf.dwFourCC == 0x30315844)
                {
                    HasExtendedHeader = true;
                    ExtendedHeader.dxgiFormat = (SharpDX.DXGI.Format)reader.ReadUInt32();
                    ExtendedHeader.resourceDimension = (D3D11.ResourceDimension)reader.ReadUInt32();
                    ExtendedHeader.miscFlag = reader.ReadUInt32();
                    ExtendedHeader.arraySize = reader.ReadUInt32();
                    ExtendedHeader.miscFlags2 = reader.ReadUInt32();
                }

                return true;
            }
        }
    }
}
