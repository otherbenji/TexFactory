﻿namespace TexFactory
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.FileListPanel = new System.Windows.Forms.Panel();
            this.TextureFilesLabel = new System.Windows.Forms.Label();
            this.FileListBorderPanel = new System.Windows.Forms.Panel();
            this.FileList = new TexFactory.Controls.FactoryListBox();
            this.RemoveFileButton = new TexFactory.Controls.FactoryButton();
            this.AddFolderButton = new TexFactory.Controls.FactoryButton();
            this.AddFileButton = new TexFactory.Controls.FactoryButton();
            this.ConvertButton = new TexFactory.Controls.FactoryButton();
            this.FileListToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.HeightTextBox = new System.Windows.Forms.TextBox();
            this.WidthTextBox = new System.Windows.Forms.TextBox();
            this.ResizeCheckBox = new TexFactory.Controls.FactoryCheckBox();
            this.OutputFormatComboBox = new TexFactory.Controls.FactoryDropDown();
            this.RemoveSuccessfulFilesCheckBox = new TexFactory.Controls.FactoryCheckBox();
            this.AdapterComboBox = new TexFactory.Controls.FactoryDropDown();
            this.GenerateMipmapsCheckBox = new TexFactory.Controls.FactoryCheckBox();
            this.OutputBrowseButton = new TexFactory.Controls.FactoryButton();
            this.OutputSRGBCheckBox = new TexFactory.Controls.FactoryCheckBox();
            this.InputSRGBCheckBox = new TexFactory.Controls.FactoryCheckBox();
            this.SelectedFileLabel = new System.Windows.Forms.Label();
            this.ConversionPanel = new System.Windows.Forms.Panel();
            this.AbortTextureButton = new TexFactory.Controls.FactoryButton();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.ConversionProgressBar = new System.Windows.Forms.ProgressBar();
            this.ConversionOutputTextBox = new System.Windows.Forms.TextBox();
            this.CurrentCommandLabel = new System.Windows.Forms.Label();
            this.CurrentFileLabel = new System.Windows.Forms.Label();
            this.ConvertingLabel = new System.Windows.Forms.Label();
            this.OptionsPanel = new System.Windows.Forms.Panel();
            this.HardwareAccelerationLabel = new System.Windows.Forms.Label();
            this.TexFactoryOptionsLabel = new System.Windows.Forms.Label();
            this.OutputFilenameTextBox = new System.Windows.Forms.TextBox();
            this.OutputLabel = new System.Windows.Forms.Label();
            this.OutputFormatLabel = new System.Windows.Forms.Label();
            this.ConversionOptionsLabel = new System.Windows.Forms.Label();
            this.InstructionLabel = new System.Windows.Forms.Label();
            this.MainSplitter = new TexFactory.Controls.FactorySplitter();
            this.FileListPanel.SuspendLayout();
            this.FileListBorderPanel.SuspendLayout();
            this.ConversionPanel.SuspendLayout();
            this.OptionsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // FileListPanel
            // 
            this.FileListPanel.Controls.Add(this.TextureFilesLabel);
            this.FileListPanel.Controls.Add(this.FileListBorderPanel);
            this.FileListPanel.Controls.Add(this.RemoveFileButton);
            this.FileListPanel.Controls.Add(this.AddFolderButton);
            this.FileListPanel.Controls.Add(this.AddFileButton);
            this.FileListPanel.Controls.Add(this.ConvertButton);
            this.FileListPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.FileListPanel.Location = new System.Drawing.Point(0, 0);
            this.FileListPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.FileListPanel.Name = "FileListPanel";
            this.FileListPanel.Size = new System.Drawing.Size(393, 737);
            this.FileListPanel.TabIndex = 0;
            // 
            // TextureFilesLabel
            // 
            this.TextureFilesLabel.AutoSize = true;
            this.TextureFilesLabel.Font = new System.Drawing.Font("Segoe UI Light", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextureFilesLabel.Location = new System.Drawing.Point(-3, 9);
            this.TextureFilesLabel.Name = "TextureFilesLabel";
            this.TextureFilesLabel.Size = new System.Drawing.Size(195, 47);
            this.TextureFilesLabel.TabIndex = 4;
            this.TextureFilesLabel.Text = "Texture Files";
            // 
            // FileListBorderPanel
            // 
            this.FileListBorderPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileListBorderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(100)))), ((int)(((byte)(100)))));
            this.FileListBorderPanel.Controls.Add(this.FileList);
            this.FileListBorderPanel.Location = new System.Drawing.Point(3, 99);
            this.FileListBorderPanel.Name = "FileListBorderPanel";
            this.FileListBorderPanel.Size = new System.Drawing.Size(387, 590);
            this.FileListBorderPanel.TabIndex = 6;
            // 
            // FileList
            // 
            this.FileList.AllowDrop = true;
            this.FileList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.FileList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FileList.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.FileList.ForeColor = System.Drawing.Color.Gainsboro;
            this.FileList.IntegralHeight = false;
            this.FileList.ItemHeight = 20;
            this.FileList.Location = new System.Drawing.Point(1, 1);
            this.FileList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.FileList.Name = "FileList";
            this.FileList.Size = new System.Drawing.Size(385, 588);
            this.FileList.TabIndex = 3;
            this.FileList.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.FileList_DrawItem);
            this.FileList.SelectedIndexChanged += new System.EventHandler(this.FileList_SelectedIndexChanged);
            this.FileList.DragDrop += new System.Windows.Forms.DragEventHandler(this.FileList_DragDrop);
            this.FileList.DragEnter += new System.Windows.Forms.DragEventHandler(this.FileList_DragEnter);
            this.FileList.MouseLeave += new System.EventHandler(this.FileList_MouseLeave);
            this.FileList.MouseHover += new System.EventHandler(this.FileList_MouseHover);
            this.FileList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FileList_MouseMove);
            // 
            // RemoveFileButton
            // 
            this.RemoveFileButton.Enabled = false;
            this.RemoveFileButton.Location = new System.Drawing.Point(209, 66);
            this.RemoveFileButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RemoveFileButton.Name = "RemoveFileButton";
            this.RemoveFileButton.Size = new System.Drawing.Size(99, 30);
            this.RemoveFileButton.TabIndex = 2;
            this.RemoveFileButton.Text = "Remove File";
            this.FileListToolTip.SetToolTip(this.RemoveFileButton, "Remove the selected file from the conversion queue.");
            this.RemoveFileButton.UseVisualStyleBackColor = true;
            this.RemoveFileButton.Click += new System.EventHandler(this.RemoveFileButton_Click);
            // 
            // AddFolderButton
            // 
            this.AddFolderButton.Location = new System.Drawing.Point(106, 66);
            this.AddFolderButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AddFolderButton.Name = "AddFolderButton";
            this.AddFolderButton.Size = new System.Drawing.Size(99, 30);
            this.AddFolderButton.TabIndex = 1;
            this.AddFolderButton.Text = "Add Folder...";
            this.FileListToolTip.SetToolTip(this.AddFolderButton, "Add all the files in a folder to the conversion queue.");
            this.AddFolderButton.UseVisualStyleBackColor = true;
            this.AddFolderButton.Click += new System.EventHandler(this.AddFolderButton_Click);
            // 
            // AddFileButton
            // 
            this.AddFileButton.Location = new System.Drawing.Point(3, 66);
            this.AddFileButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AddFileButton.Name = "AddFileButton";
            this.AddFileButton.Size = new System.Drawing.Size(99, 30);
            this.AddFileButton.TabIndex = 0;
            this.AddFileButton.Text = "Add File...";
            this.FileListToolTip.SetToolTip(this.AddFileButton, "Add a texture file to the conversion queue.");
            this.AddFileButton.UseVisualStyleBackColor = true;
            this.AddFileButton.Click += new System.EventHandler(this.AddFileButton_Click);
            // 
            // ConvertButton
            // 
            this.ConvertButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ConvertButton.Enabled = false;
            this.ConvertButton.Location = new System.Drawing.Point(3, 692);
            this.ConvertButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ConvertButton.Name = "ConvertButton";
            this.ConvertButton.Size = new System.Drawing.Size(387, 42);
            this.ConvertButton.TabIndex = 4;
            this.ConvertButton.Text = "Convert 0 Files";
            this.FileListToolTip.SetToolTip(this.ConvertButton, "Begin the batch conversion.");
            this.ConvertButton.UseVisualStyleBackColor = true;
            this.ConvertButton.Click += new System.EventHandler(this.ConvertButton_Click);
            // 
            // FileListToolTip
            // 
            this.FileListToolTip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.FileListToolTip.ForeColor = System.Drawing.Color.Gainsboro;
            this.FileListToolTip.OwnerDraw = true;
            this.FileListToolTip.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.FileListToolTip_Draw);
            // 
            // HeightTextBox
            // 
            this.HeightTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.HeightTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HeightTextBox.ForeColor = System.Drawing.Color.Gainsboro;
            this.HeightTextBox.Location = new System.Drawing.Point(166, 232);
            this.HeightTextBox.Multiline = true;
            this.HeightTextBox.Name = "HeightTextBox";
            this.HeightTextBox.Size = new System.Drawing.Size(69, 23);
            this.HeightTextBox.TabIndex = 13;
            this.FileListToolTip.SetToolTip(this.HeightTextBox, "Height of the output texture.");
            this.HeightTextBox.TextChanged += new System.EventHandler(this.HeightTextBox_TextChanged);
            // 
            // WidthTextBox
            // 
            this.WidthTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.WidthTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WidthTextBox.ForeColor = System.Drawing.Color.Gainsboro;
            this.WidthTextBox.Location = new System.Drawing.Point(91, 232);
            this.WidthTextBox.Multiline = true;
            this.WidthTextBox.Name = "WidthTextBox";
            this.WidthTextBox.Size = new System.Drawing.Size(69, 23);
            this.WidthTextBox.TabIndex = 12;
            this.FileListToolTip.SetToolTip(this.WidthTextBox, "Width of the output texture.");
            this.WidthTextBox.TextChanged += new System.EventHandler(this.WidthTextBox_TextChanged);
            // 
            // ResizeCheckBox
            // 
            this.ResizeCheckBox.AutoSize = true;
            this.ResizeCheckBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResizeCheckBox.Location = new System.Drawing.Point(4, 233);
            this.ResizeCheckBox.Name = "ResizeCheckBox";
            this.ResizeCheckBox.Size = new System.Drawing.Size(74, 23);
            this.ResizeCheckBox.TabIndex = 11;
            this.ResizeCheckBox.Text = "Resize";
            this.FileListToolTip.SetToolTip(this.ResizeCheckBox, "Resize the output to the given size.");
            this.ResizeCheckBox.UseVisualStyleBackColor = true;
            this.ResizeCheckBox.CheckedChanged += new System.EventHandler(this.ResizeCheckBox_CheckedChanged);
            // 
            // OutputFormatComboBox
            // 
            this.OutputFormatComboBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputFormatComboBox.Location = new System.Drawing.Point(122, 176);
            this.OutputFormatComboBox.Name = "OutputFormatComboBox";
            this.OutputFormatComboBox.SelectedIndex = -1;
            this.OutputFormatComboBox.SelectedItem = null;
            this.OutputFormatComboBox.Size = new System.Drawing.Size(405, 29);
            this.OutputFormatComboBox.TabIndex = 9;
            this.FileListToolTip.SetToolTip(this.OutputFormatComboBox, "The pixel format the data should be saved in. (DDS)");
            this.OutputFormatComboBox.SelectedIndexChanged += new System.EventHandler(this.OutputFormatComboBox_SelectedIndexChanged);
            // 
            // RemoveSuccessfulFilesCheckBox
            // 
            this.RemoveSuccessfulFilesCheckBox.AutoSize = true;
            this.RemoveSuccessfulFilesCheckBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RemoveSuccessfulFilesCheckBox.Location = new System.Drawing.Point(4, 361);
            this.RemoveSuccessfulFilesCheckBox.Name = "RemoveSuccessfulFilesCheckBox";
            this.RemoveSuccessfulFilesCheckBox.Size = new System.Drawing.Size(287, 23);
            this.RemoveSuccessfulFilesCheckBox.TabIndex = 15;
            this.RemoveSuccessfulFilesCheckBox.Text = "Remove successfully converted files";
            this.FileListToolTip.SetToolTip(this.RemoveSuccessfulFilesCheckBox, "Remove files from the conversion queue when they finish conversion successfully.");
            this.RemoveSuccessfulFilesCheckBox.UseVisualStyleBackColor = true;
            this.RemoveSuccessfulFilesCheckBox.CheckedChanged += new System.EventHandler(this.RemoveSuccessfulFilesCheckBox_CheckedChanged);
            // 
            // AdapterComboBox
            // 
            this.AdapterComboBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdapterComboBox.Location = new System.Drawing.Point(177, 331);
            this.AdapterComboBox.Name = "AdapterComboBox";
            this.AdapterComboBox.SelectedIndex = -1;
            this.AdapterComboBox.SelectedItem = null;
            this.AdapterComboBox.Size = new System.Drawing.Size(350, 29);
            this.AdapterComboBox.TabIndex = 14;
            this.FileListToolTip.SetToolTip(this.AdapterComboBox, "The graphics adapter that should be used to speed up calculations.");
            this.AdapterComboBox.SelectedIndexChanged += new System.EventHandler(this.AdapterComboBox_SelectedIndexChanged);
            // 
            // GenerateMipmapsCheckBox
            // 
            this.GenerateMipmapsCheckBox.AutoSize = true;
            this.GenerateMipmapsCheckBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GenerateMipmapsCheckBox.Location = new System.Drawing.Point(4, 205);
            this.GenerateMipmapsCheckBox.Name = "GenerateMipmapsCheckBox";
            this.GenerateMipmapsCheckBox.Size = new System.Drawing.Size(167, 23);
            this.GenerateMipmapsCheckBox.TabIndex = 10;
            this.GenerateMipmapsCheckBox.Text = "Generate mipmaps";
            this.FileListToolTip.SetToolTip(this.GenerateMipmapsCheckBox, "Generate mipmaps (smaller copies). (DDS)");
            this.GenerateMipmapsCheckBox.UseVisualStyleBackColor = true;
            this.GenerateMipmapsCheckBox.CheckedChanged += new System.EventHandler(this.GenerateMipmapsCheckBox_CheckedChanged);
            // 
            // OutputBrowseButton
            // 
            this.OutputBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OutputBrowseButton.Location = new System.Drawing.Point(1413, 96);
            this.OutputBrowseButton.Name = "OutputBrowseButton";
            this.OutputBrowseButton.Size = new System.Drawing.Size(31, 23);
            this.OutputBrowseButton.TabIndex = 6;
            this.OutputBrowseButton.Text = "...";
            this.FileListToolTip.SetToolTip(this.OutputBrowseButton, "Pick a new output filename.");
            this.OutputBrowseButton.UseVisualStyleBackColor = true;
            this.OutputBrowseButton.Click += new System.EventHandler(this.OutputBrowseButton_Click);
            // 
            // OutputSRGBCheckBox
            // 
            this.OutputSRGBCheckBox.AutoSize = true;
            this.OutputSRGBCheckBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputSRGBCheckBox.Location = new System.Drawing.Point(4, 151);
            this.OutputSRGBCheckBox.Name = "OutputSRGBCheckBox";
            this.OutputSRGBCheckBox.Size = new System.Drawing.Size(138, 23);
            this.OutputSRGBCheckBox.TabIndex = 8;
            this.OutputSRGBCheckBox.Text = "Output is sRGB";
            this.FileListToolTip.SetToolTip(this.OutputSRGBCheckBox, "Treat the output texture as data in the sRGB color space.");
            this.OutputSRGBCheckBox.UseVisualStyleBackColor = true;
            this.OutputSRGBCheckBox.CheckedChanged += new System.EventHandler(this.OutputSRGBCheckBox_CheckedChanged);
            // 
            // InputSRGBCheckBox
            // 
            this.InputSRGBCheckBox.AutoSize = true;
            this.InputSRGBCheckBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InputSRGBCheckBox.Location = new System.Drawing.Point(4, 122);
            this.InputSRGBCheckBox.Name = "InputSRGBCheckBox";
            this.InputSRGBCheckBox.Size = new System.Drawing.Size(125, 23);
            this.InputSRGBCheckBox.TabIndex = 7;
            this.InputSRGBCheckBox.Text = "Input is sRGB";
            this.FileListToolTip.SetToolTip(this.InputSRGBCheckBox, "Treat the input texture as data in the sRGB color space.");
            this.InputSRGBCheckBox.UseVisualStyleBackColor = true;
            this.InputSRGBCheckBox.CheckedChanged += new System.EventHandler(this.InputSRGBCheckBox_CheckedChanged);
            // 
            // SelectedFileLabel
            // 
            this.SelectedFileLabel.AutoSize = true;
            this.SelectedFileLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectedFileLabel.Location = new System.Drawing.Point(0, 70);
            this.SelectedFileLabel.Name = "SelectedFileLabel";
            this.SelectedFileLabel.Size = new System.Drawing.Size(139, 21);
            this.SelectedFileLabel.TabIndex = 6;
            this.SelectedFileLabel.Text = "[SelectedFileLabel]";
            this.FileListToolTip.SetToolTip(this.SelectedFileLabel, "Filename of the source texture. (Click to open)");
            this.SelectedFileLabel.Click += new System.EventHandler(this.SelectedFileLabel_Click);
            // 
            // ConversionPanel
            // 
            this.ConversionPanel.Controls.Add(this.AbortTextureButton);
            this.ConversionPanel.Controls.Add(this.StatusLabel);
            this.ConversionPanel.Controls.Add(this.ConversionProgressBar);
            this.ConversionPanel.Controls.Add(this.ConversionOutputTextBox);
            this.ConversionPanel.Controls.Add(this.CurrentCommandLabel);
            this.ConversionPanel.Controls.Add(this.CurrentFileLabel);
            this.ConversionPanel.Controls.Add(this.ConvertingLabel);
            this.ConversionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ConversionPanel.Location = new System.Drawing.Point(398, 0);
            this.ConversionPanel.Name = "ConversionPanel";
            this.ConversionPanel.Size = new System.Drawing.Size(1049, 737);
            this.ConversionPanel.TabIndex = 3;
            this.ConversionPanel.Visible = false;
            // 
            // AbortTextureButton
            // 
            this.AbortTextureButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AbortTextureButton.Location = new System.Drawing.Point(831, 692);
            this.AbortTextureButton.Name = "AbortTextureButton";
            this.AbortTextureButton.Size = new System.Drawing.Size(215, 42);
            this.AbortTextureButton.TabIndex = 6;
            this.AbortTextureButton.Text = "Abort Texture";
            this.FileListToolTip.SetToolTip(this.AbortTextureButton, "Cancel the conversion of the current file.");
            this.AbortTextureButton.UseVisualStyleBackColor = true;
            this.AbortTextureButton.Click += new System.EventHandler(this.AbortTextureButton_Click);
            // 
            // StatusLabel
            // 
            this.StatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusLabel.Location = new System.Drawing.Point(-1, 705);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(99, 21);
            this.StatusLabel.TabIndex = 5;
            this.StatusLabel.Text = "[StatusLabel]";
            // 
            // ConversionProgressBar
            // 
            this.ConversionProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ConversionProgressBar.Location = new System.Drawing.Point(3, 692);
            this.ConversionProgressBar.Name = "ConversionProgressBar";
            this.ConversionProgressBar.Size = new System.Drawing.Size(822, 10);
            this.ConversionProgressBar.TabIndex = 4;
            // 
            // ConversionOutputTextBox
            // 
            this.ConversionOutputTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ConversionOutputTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.ConversionOutputTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ConversionOutputTextBox.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConversionOutputTextBox.ForeColor = System.Drawing.Color.Gainsboro;
            this.ConversionOutputTextBox.Location = new System.Drawing.Point(3, 99);
            this.ConversionOutputTextBox.Multiline = true;
            this.ConversionOutputTextBox.Name = "ConversionOutputTextBox";
            this.ConversionOutputTextBox.ReadOnly = true;
            this.ConversionOutputTextBox.Size = new System.Drawing.Size(1043, 590);
            this.ConversionOutputTextBox.TabIndex = 0;
            this.ConversionOutputTextBox.TabStop = false;
            this.ConversionOutputTextBox.Text = "[ConversionOutputTextBox]";
            this.FileListToolTip.SetToolTip(this.ConversionOutputTextBox, "The output from the texconv converter.");
            // 
            // CurrentCommandLabel
            // 
            this.CurrentCommandLabel.AutoSize = true;
            this.CurrentCommandLabel.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentCommandLabel.Location = new System.Drawing.Point(-1, 77);
            this.CurrentCommandLabel.Name = "CurrentCommandLabel";
            this.CurrentCommandLabel.Size = new System.Drawing.Size(198, 19);
            this.CurrentCommandLabel.TabIndex = 2;
            this.CurrentCommandLabel.Text = "[CurrentCommandLabel]";
            this.FileListToolTip.SetToolTip(this.CurrentCommandLabel, "The command that is being run.");
            // 
            // CurrentFileLabel
            // 
            this.CurrentFileLabel.AutoSize = true;
            this.CurrentFileLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentFileLabel.Location = new System.Drawing.Point(0, 56);
            this.CurrentFileLabel.Name = "CurrentFileLabel";
            this.CurrentFileLabel.Size = new System.Drawing.Size(134, 21);
            this.CurrentFileLabel.TabIndex = 1;
            this.CurrentFileLabel.Text = "[CurrentFileLabel]";
            this.FileListToolTip.SetToolTip(this.CurrentFileLabel, "The filename of the texture in progress.");
            // 
            // ConvertingLabel
            // 
            this.ConvertingLabel.AutoSize = true;
            this.ConvertingLabel.Font = new System.Drawing.Font("Segoe UI Light", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConvertingLabel.Location = new System.Drawing.Point(-6, 9);
            this.ConvertingLabel.Name = "ConvertingLabel";
            this.ConvertingLabel.Size = new System.Drawing.Size(185, 47);
            this.ConvertingLabel.TabIndex = 0;
            this.ConvertingLabel.Text = "Converting";
            // 
            // OptionsPanel
            // 
            this.OptionsPanel.Controls.Add(this.HeightTextBox);
            this.OptionsPanel.Controls.Add(this.WidthTextBox);
            this.OptionsPanel.Controls.Add(this.ResizeCheckBox);
            this.OptionsPanel.Controls.Add(this.OutputFormatComboBox);
            this.OptionsPanel.Controls.Add(this.RemoveSuccessfulFilesCheckBox);
            this.OptionsPanel.Controls.Add(this.HardwareAccelerationLabel);
            this.OptionsPanel.Controls.Add(this.AdapterComboBox);
            this.OptionsPanel.Controls.Add(this.TexFactoryOptionsLabel);
            this.OptionsPanel.Controls.Add(this.GenerateMipmapsCheckBox);
            this.OptionsPanel.Controls.Add(this.OutputBrowseButton);
            this.OptionsPanel.Controls.Add(this.OutputFilenameTextBox);
            this.OptionsPanel.Controls.Add(this.OutputLabel);
            this.OptionsPanel.Controls.Add(this.OutputFormatLabel);
            this.OptionsPanel.Controls.Add(this.OutputSRGBCheckBox);
            this.OptionsPanel.Controls.Add(this.InputSRGBCheckBox);
            this.OptionsPanel.Controls.Add(this.SelectedFileLabel);
            this.OptionsPanel.Controls.Add(this.ConversionOptionsLabel);
            this.OptionsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptionsPanel.Location = new System.Drawing.Point(0, 0);
            this.OptionsPanel.Name = "OptionsPanel";
            this.OptionsPanel.Size = new System.Drawing.Size(1447, 737);
            this.OptionsPanel.TabIndex = 4;
            this.OptionsPanel.Visible = false;
            // 
            // HardwareAccelerationLabel
            // 
            this.HardwareAccelerationLabel.AutoSize = true;
            this.HardwareAccelerationLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HardwareAccelerationLabel.Location = new System.Drawing.Point(0, 334);
            this.HardwareAccelerationLabel.Name = "HardwareAccelerationLabel";
            this.HardwareAccelerationLabel.Size = new System.Drawing.Size(168, 21);
            this.HardwareAccelerationLabel.TabIndex = 17;
            this.HardwareAccelerationLabel.Text = "Hardware acceleration:";
            // 
            // TexFactoryOptionsLabel
            // 
            this.TexFactoryOptionsLabel.AutoSize = true;
            this.TexFactoryOptionsLabel.Font = new System.Drawing.Font("Segoe UI Light", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TexFactoryOptionsLabel.Location = new System.Drawing.Point(-8, 268);
            this.TexFactoryOptionsLabel.Name = "TexFactoryOptionsLabel";
            this.TexFactoryOptionsLabel.Size = new System.Drawing.Size(299, 47);
            this.TexFactoryOptionsLabel.TabIndex = 0;
            this.TexFactoryOptionsLabel.Text = "TexFactory Options";
            // 
            // OutputFilenameTextBox
            // 
            this.OutputFilenameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OutputFilenameTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.OutputFilenameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.OutputFilenameTextBox.ForeColor = System.Drawing.Color.Gainsboro;
            this.OutputFilenameTextBox.Location = new System.Drawing.Point(68, 96);
            this.OutputFilenameTextBox.Multiline = true;
            this.OutputFilenameTextBox.Name = "OutputFilenameTextBox";
            this.OutputFilenameTextBox.Size = new System.Drawing.Size(1342, 23);
            this.OutputFilenameTextBox.TabIndex = 5;
            this.OutputFilenameTextBox.TextChanged += new System.EventHandler(this.OutputFilenameTextBox_TextChanged);
            // 
            // OutputLabel
            // 
            this.OutputLabel.AutoSize = true;
            this.OutputLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputLabel.Location = new System.Drawing.Point(0, 96);
            this.OutputLabel.Name = "OutputLabel";
            this.OutputLabel.Size = new System.Drawing.Size(62, 21);
            this.OutputLabel.TabIndex = 11;
            this.OutputLabel.Text = "Output:";
            // 
            // OutputFormatLabel
            // 
            this.OutputFormatLabel.AutoSize = true;
            this.OutputFormatLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputFormatLabel.Location = new System.Drawing.Point(0, 180);
            this.OutputFormatLabel.Name = "OutputFormatLabel";
            this.OutputFormatLabel.Size = new System.Drawing.Size(113, 21);
            this.OutputFormatLabel.TabIndex = 0;
            this.OutputFormatLabel.Text = "Output format:";
            // 
            // ConversionOptionsLabel
            // 
            this.ConversionOptionsLabel.AutoSize = true;
            this.ConversionOptionsLabel.Font = new System.Drawing.Font("Segoe UI Light", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConversionOptionsLabel.Location = new System.Drawing.Point(-6, 9);
            this.ConversionOptionsLabel.Name = "ConversionOptionsLabel";
            this.ConversionOptionsLabel.Size = new System.Drawing.Size(314, 47);
            this.ConversionOptionsLabel.TabIndex = 0;
            this.ConversionOptionsLabel.Text = "Conversion Options";
            // 
            // InstructionLabel
            // 
            this.InstructionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InstructionLabel.Font = new System.Drawing.Font("Segoe UI Light", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstructionLabel.Image = global::TexFactory.Properties.Resources.Watermark;
            this.InstructionLabel.Location = new System.Drawing.Point(398, 0);
            this.InstructionLabel.Name = "InstructionLabel";
            this.InstructionLabel.Padding = new System.Windows.Forms.Padding(0, 0, 0, 30);
            this.InstructionLabel.Size = new System.Drawing.Size(1049, 737);
            this.InstructionLabel.TabIndex = 2;
            this.InstructionLabel.Text = "Add texture files to get started";
            this.InstructionLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // MainSplitter
            // 
            this.MainSplitter.Location = new System.Drawing.Point(393, 0);
            this.MainSplitter.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MainSplitter.Name = "MainSplitter";
            this.MainSplitter.Size = new System.Drawing.Size(5, 737);
            this.MainSplitter.TabIndex = 1;
            this.MainSplitter.TabStop = false;
            // 
            // Window
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(1447, 737);
            this.Controls.Add(this.OptionsPanel);
            this.Controls.Add(this.ConversionPanel);
            this.Controls.Add(this.InstructionLabel);
            this.Controls.Add(this.MainSplitter);
            this.Controls.Add(this.FileListPanel);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Gainsboro;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Window";
            this.Text = "TexFactory";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Window_FormClosing);
            this.Load += new System.EventHandler(this.Window_Load);
            this.FileListPanel.ResumeLayout(false);
            this.FileListPanel.PerformLayout();
            this.FileListBorderPanel.ResumeLayout(false);
            this.ConversionPanel.ResumeLayout(false);
            this.ConversionPanel.PerformLayout();
            this.OptionsPanel.ResumeLayout(false);
            this.OptionsPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel FileListPanel;
        private TexFactory.Controls.FactorySplitter MainSplitter;
        private TexFactory.Controls.FactoryButton ConvertButton;
        private TexFactory.Controls.FactoryButton RemoveFileButton;
        private TexFactory.Controls.FactoryButton AddFolderButton;
        private TexFactory.Controls.FactoryButton AddFileButton;
        private TexFactory.Controls.FactoryListBox FileList;
        private System.Windows.Forms.Label InstructionLabel;
        private System.Windows.Forms.Panel FileListBorderPanel;
        private System.Windows.Forms.ToolTip FileListToolTip;
        private System.Windows.Forms.Panel ConversionPanel;
        private System.Windows.Forms.Label ConvertingLabel;
        private System.Windows.Forms.Label CurrentFileLabel;
        private System.Windows.Forms.Label CurrentCommandLabel;
        private System.Windows.Forms.TextBox ConversionOutputTextBox;
        private System.Windows.Forms.Label TextureFilesLabel;
        private System.Windows.Forms.ProgressBar ConversionProgressBar;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Panel OptionsPanel;
        private System.Windows.Forms.Label ConversionOptionsLabel;
        private System.Windows.Forms.Label SelectedFileLabel;
        private TexFactory.Controls.FactoryCheckBox InputSRGBCheckBox;
        private TexFactory.Controls.FactoryCheckBox OutputSRGBCheckBox;
        private System.Windows.Forms.Label OutputFormatLabel;
        private TexFactory.Controls.FactoryDropDown OutputFormatComboBox;
        private System.Windows.Forms.Label OutputLabel;
        private System.Windows.Forms.TextBox OutputFilenameTextBox;
        private TexFactory.Controls.FactoryButton OutputBrowseButton;
        private TexFactory.Controls.FactoryCheckBox GenerateMipmapsCheckBox;
        private TexFactory.Controls.FactoryButton AbortTextureButton;
        private System.Windows.Forms.Label HardwareAccelerationLabel;
        private TexFactory.Controls.FactoryDropDown AdapterComboBox;
        private System.Windows.Forms.Label TexFactoryOptionsLabel;
        private TexFactory.Controls.FactoryCheckBox RemoveSuccessfulFilesCheckBox;
        private System.Windows.Forms.TextBox HeightTextBox;
        private System.Windows.Forms.TextBox WidthTextBox;
        private TexFactory.Controls.FactoryCheckBox ResizeCheckBox;
    }
}

