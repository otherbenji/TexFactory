﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TexFactory
{
    public class TextureItem
    {
        public enum ConversionState
        {
            None = 0,
            Success = 1,
            Error = 2,
            Skip = 3,
            Processing = 4
        }

        private class TextureMetadata
        {
            private const string StreamName = "TexHelperMetadata";

            /// <summary>
            /// The pixel format of the image that the file described by this instance was created from.
            /// </summary>
            public SharpDX.DXGI.Format OriginalPixelFormat = SharpDX.DXGI.Format.Unknown;

            /// <summary>
            /// Whether the image that the file described by this instance was created from was in the sRGB color space.
            /// </summary>
            public bool WasSRGB = false;

            /// <summary>
            /// Whether the color data contined in the file described by this instance is in the sRGB color space.
            /// </summary>
            public bool IsSRGB = false;

            /// <summary>
            /// Creates a new <see cref="TextureMetadata"/> instance with the default values.
            /// </summary>
            public TextureMetadata()
            {

            }

            /// <summary>
            /// Loads the metadata from the give stream.
            /// </summary>
            /// <param name="streamname">The filename (including NTFS alternate data stream) to load from.</param>
            private TextureMetadata(string streamname)
            {
                using (Microsoft.Win32.SafeHandles.SafeFileHandle handle = Controls.Win32.CreateFile(streamname, Controls.Win32.EFileAccess.GenericRead, Controls.Win32.EFileShare.Read, IntPtr.Zero, Controls.Win32.EFileMode.OpenExisting, 0, IntPtr.Zero))
                {
                    if (handle.IsInvalid)
                    {
                        int error = System.Runtime.InteropServices.Marshal.GetLastWin32Error();
                        System.ComponentModel.Win32Exception exception = new System.ComponentModel.Win32Exception(error);
                        System.Windows.Forms.MessageBox.Show("Exception opening metadata file: \n\n" + exception.ToString());
                        throw exception;
                    }
                    else
                    {
                        using (System.IO.FileStream stream = new System.IO.FileStream(handle, System.IO.FileAccess.Read))
                        {
                            INIFile ini = new INIFile(stream);
                            WasSRGB = ini.GetValueOrDefault("TexFactoryMeta", "WasSRGB", "False").Equals("True", StringComparison.CurrentCultureIgnoreCase);
                            IsSRGB = ini.GetValueOrDefault("TexFactoryMeta", "IsSRGB", "False").Equals("True", StringComparison.CurrentCultureIgnoreCase);
                            if (Enum.TryParse(ini.GetValueOrDefault("TexFactoryMeta", "OriginalPixelFormat", SharpDX.DXGI.Format.Unknown.ToString()), true, out SharpDX.DXGI.Format format))
                            {
                                OriginalPixelFormat = format;
                            }
                        }
                        //System.Windows.Forms.MessageBox.Show("Metadata read complete.");
                    }
                }
            }

            public void Write(string filename)
            {
                //System.Security.Permissions.FileIOPermission perm = new System.Security.Permissions.FileIOPermission(System.Security.Permissions.PermissionState.None);
                //perm.AllFiles = System.Security.Permissions.FileIOPermissionAccess.AllAccess;
                //perm.Demand();
                string streamname = filename + ":" + StreamName;
                if (Controls.Win32.PathFileExists(streamname))
                {
                    //System.Windows.Forms.MessageBox.Show("Deleting existing metadata.");
                    Controls.Win32.DeleteFile(streamname);
                }
                using (Microsoft.Win32.SafeHandles.SafeFileHandle handle = Controls.Win32.CreateFile(streamname, Controls.Win32.EFileAccess.GenericWrite, Controls.Win32.EFileShare.None, IntPtr.Zero, Controls.Win32.EFileMode.CreateAlways, Controls.Win32.EFileAttributes.Normal, IntPtr.Zero))
                {
                    if (handle.IsInvalid)
                    {
                        int error = System.Runtime.InteropServices.Marshal.GetLastWin32Error();
                        System.ComponentModel.Win32Exception exception = new System.ComponentModel.Win32Exception(error);
                        if (exception.ToString().Contains("denied"))
                        {
                            System.Windows.Forms.MessageBox.Show("Access denied. Existing file attributes: " + System.IO.File.GetAttributes(filename).ToString());
                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("Error: " + exception.ToString());
                        }
                        throw exception;
                    }
                    else
                    {
                        using (System.IO.FileStream stream = new System.IO.FileStream(handle, System.IO.FileAccess.Write))
                        {
                            INIFile ini = new INIFile();
                            ini.SetValue("TexFactoryMeta", "WasSRGB", WasSRGB ? "True" : "False");
                            ini.SetValue("TexFactoryMeta", "OriginalPixelFormat", OriginalPixelFormat.ToString());
                            ini.SetValue("TexFactoryMeta", "IsSRGB", IsSRGB ? "True" : "False");
                            ini.Write(stream);
                        }
                    }
                }
            }

            public static bool Exists(string filename)
            {
                return Controls.Win32.PathFileExists(filename + ":" + StreamName);
            }

            public static TextureMetadata ReadMetadata(string filename)
            {
                if (!Exists(filename))
                {
                    //System.Windows.Forms.MessageBox.Show("Trying to get metadata for file '" + filename + "' that does not exist!!!");
                    return null;
                }
                else
                {
                    try
                    {
                        return new TextureMetadata(filename + ":" + StreamName);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLine("[WARNING]: Exception reading metadata for '" + filename + "':");
                        Logger.WriteLine(ex.ToString());
                        System.Windows.Forms.MessageBox.Show("Exception reading metadata: \n" + ex.ToString());
                        return null;
                    }
                }
            }
        }

        public string Filename { get; }
        public string OutputFilename { get; set; }
        public ConversionState State { get; set; } = ConversionState.None;
        public string StateMessage { get; set; }

        public bool IsInputSRGB { get; set; }
        public bool IsOutputSRGB { get; set; }
        public SharpDX.DXGI.Format OutputFormat { get; set; } = SharpDX.DXGI.Format.Unknown;
        public bool GenerateMipmaps { get; set; } = true;
        public bool Resize { get; set; } = false;
        public int ResizeWidth { get; set; } = 0;
        public int ResizeHeight { get; set; } = 0;

        public TextureItem(string filename, string outputFilename)
        {
            Filename = filename;
            OutputFilename = outputFilename;

            // Load the metadata
            TextureMetadata data = TextureMetadata.ReadMetadata(filename);
            if (data != null)
            {
                //System.Windows.Forms.MessageBox.Show("data != null\n\nIsSRGB:" + data.IsSRGB.ToString() + "\nWasSRGB:" + data.WasSRGB.ToString() + "\nOriginalPixelFormat:" + data.OriginalPixelFormat.ToString());
                IsInputSRGB = data.IsSRGB;
                IsOutputSRGB = data.WasSRGB;
                OutputFormat = data.OriginalPixelFormat;
            }
            else if (Filename.EndsWith("dds", StringComparison.CurrentCultureIgnoreCase))
            {
                //System.Windows.Forms.MessageBox.Show("data == null, trying for DDS header.");
                // Try getting sRGB status from the DDS header
                try
                {
                    using (System.IO.FileStream stream = new System.IO.FileStream(Filename, System.IO.FileMode.Open))
                    using (System.IO.BinaryReader reader = new System.IO.BinaryReader(stream))
                    {
                        DDSFile.DDSHeader header = new DDSFile.DDSHeader();
                        if (header.Read(reader) && header.HasExtendedHeader)
                        {
                            IsInputSRGB = IsOutputSRGB = SharpDX.DXGI.FormatHelper.IsSRgb(header.ExtendedHeader.dxgiFormat);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLine("[WARNING]: Exception reading DDS header for '" + Filename + "':");
                    Logger.WriteLine(ex.ToString());
                }
                OutputFormat = SharpDX.DXGI.Format.B8G8R8A8_UNorm;
            }
            else
            {
                //System.Windows.Forms.MessageBox.Show("data == null. giving up.");
            }
            //System.Windows.Forms.MessageBox.Show("Format: " + OutputFormat.ToString());
        }

        public void StoreResultingMetadata()
        {
            // Create a new metadata, populate it from this info, and write it
            TextureMetadata data = new TextureMetadata();
            data.WasSRGB = IsInputSRGB;
            data.IsSRGB = IsOutputSRGB;

            // See if we can store the DDS pixel format
            if (Filename.EndsWith("dds", StringComparison.CurrentCultureIgnoreCase))
            {
                try
                {
                    using (System.IO.FileStream stream = new System.IO.FileStream(Filename, System.IO.FileMode.Open))
                    using (System.IO.BinaryReader reader = new System.IO.BinaryReader(stream))
                    {
                        DDSFile.DDSHeader header = new DDSFile.DDSHeader();
                        if (header.Read(reader) && header.HasExtendedHeader)
                        {
                            data.OriginalPixelFormat = header.ExtendedHeader.dxgiFormat;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLine("[WARNING]: Exception reading DDS header for '" + Filename + "':");
                    Logger.WriteLine(ex.ToString());
                }
            }

            try
            {
                data.Write(OutputFilename);
            }
            catch (Exception ex)
            {
                Logger.WriteLine("[WARNING]: Exception writing metadata for '" + OutputFilename + "'. Details: ");
                Logger.WriteLine(ex.ToString());
            }
        }
    }
}
