[TexFactory](https://otherbenji.gitlab.io/TexFactory/)
==========

Straightforward GUI for [texconv](https://github.com/Microsoft/DirectXTex/wiki/Texconv).
Makes converting textures between `.TGA` or `.PNG` and modern `.DDS` a breeze.

Usage
-----
 - Open TexFactory.
 - Add texture files by dragging them onto the list, or by clicking the `Add File` or `Add Folder` buttons.
 - Configure each conversion by selecting the texture file in the list, and then setting the appropriate options.
 - Start the conversions by clicking the `Convert Files` button.
 - If any files don't convert correctly, you can hover over them in the list to see a description of the issue.

TODO:
-----
 - [ ] Show a texture preview
 - [ ] Store mipmap status in `TextureMetadata`